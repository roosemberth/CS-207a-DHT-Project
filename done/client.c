#include <errno.h>
#include <stddef.h>
#include <stdlib.h>

#include "args.h"
#include "ring.h"
#include "client.h"
#include "config.h"
#include "error.h"
#include "myutil.h"
#include "node_list.h"

void client_end(client_t *client) {
  if (client->args != NULL) {
    free(client->args);
    client->args = NULL;
  }
  if (client->ring != NULL) {
    ring_free(client->ring);
    client->ring = NULL;
  }
}

error_code client_init(client_init_args_t cli_init) {
  client_t *client = cli_init.client;

  char ***argv_ptr = cli_init.argv_ptr;
  size_t argc = cli_init.argc;

  // const char* const prog_name = argv[0];
  /*Supress program name*/
  fflush(stdout);

  ++(*argv_ptr);
  --argc;

  if (cli_init.arg_required != SIZE_MAX) {
    M_EXIT_IF(argc < cli_init.arg_required, ERR_BAD_PARAMETER, "client_init",
                      "did not respect %zu mandatory arguments",
                      cli_init.arg_required);
  }

  size_t arg_opt = argc - cli_init.arg_required;
  if (arg_opt > 0) {
    char **argv_prev = *argv_ptr;
    args_t *args = parse_opt_args(cli_init.supported_args, argv_ptr);
    M_EXIT_IF(args == NULL, ERR_IO, "client_init",
        "Error in parsing the (at most %zu) optional arguments", arg_opt);

    client->args = args;
    ptrdiff_t dp = *argv_ptr - argv_prev;
    argc -= (size_t)dp;
  } else {
    client->args = malloc(sizeof(args_t));
    M_EXIT_IF_NULL(client->args, sizeof(args_t), "client->args alloc");

    client->args->N = 3;
    client->args->R = 2;
    client->args->W = 2;
  }

  if (cli_init.arg_required != SIZE_MAX) {
    // not pps-client-cat
    M_CLOSURE_EXIT_IF(argc != cli_init.arg_required, client_end(client),
                      ERR_BAD_PARAMETER, "client_init",
                      "did not respect %zu mandatory arguments",
                      cli_init.arg_required);
  } else {
    // pps-client-cat
    M_CLOSURE_EXIT_IF(argc < 2, client_end(client), ERR_BAD_PARAMETER,
                      "client_init",
                      "did not respect at least %d mandatory arguments", 2);
  }

  /*Initialize nodes corresponding to client*/
  client->ring = ring_alloc();
  M_CLOSURE_EXIT_IF(ring_init(client->ring) != ERR_NONE, client_end(client),
                    ERR_NETWORK, "client_init", "Unable to Initialize eing: %s",
                    strerror(errno));

  int test = ((client->args->N > client->ring->nb_nodes) ||
              (client->args->R > client->args->N) ||
              (client->args->W > client->args->N));

  M_CLOSURE_EXIT_IF(test, client_end(client), ERR_BAD_PARAMETER, "client_init",
                    "(N(=%zu) > S) or (R(=%zu) > N) or (W(=%zu) > N)",
                    client->args->N, client->args->R, client->args->W);

  return ERR_NONE;
}
