#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "client.h"
#include "config.h"
#include "myutil.h"
#include "error.h"
#include "network.h"
#include "system.h"

#define true 1
#define false 0
#define socket_time 1

/** ======================================================================
 * Increase (by 1) the count for value (here taken as key) or initialize to
 * "\x01" if the value (and corresponding count) is still not in Htable
 */
static error_code increase_count(Htable_t *table, pps_key_t value)
{
  error_code err = ERR_NONE;
  pps_value_t prev_count = get_Htable_value(*table, value);
  if (prev_count == NULL) {
    pps_value_t new_count = "\x01";
    err = add_Htable_value(*table, value, new_count);
    M_EXIT_IF(err != ERR_NONE, err, "increase_count", "Could not store %s:%s",
              value, new_count);
  } else {
    char *next_count = strdup(prev_count);
    ++next_count[0];
    err = add_Htable_value(*table, value, (pps_value_t)next_count);
    free(next_count); // free memory allocated in strdup
    M_EXIT_IF(err != ERR_NONE, err, "increase_count",
              "Could not store next_count for value %s", value);
  }
  return err;
}

/** ======================================================================
 * Verify if the count for the received value already reached the quorum
 */
static int verify_quorum(Htable_t *table, pps_key_t value, size_t R)
{
  pps_value_t count = get_Htable_value(*table, value);
  char str_quorum[] = {(char)R, '\0'};
  return (strcmp(count, str_quorum) >= 0 ? 1 : 0);
}

/** ======================================================================
 * free buffer; close socket, delete node_list, delete Htable_t
 * (note: should end client in every pps-*)
 */
static void network_end(char** buf, int socket, node_list_t** n_list,
                        Htable_t* table)
{
  if((buf != NULL) && (*buf != NULL)){
    free(*buf);
    *buf = NULL;    
  }
  close(socket);
  if((n_list != NULL) && (*n_list != NULL)){
    free((*n_list)->list);
    (*n_list)->list = NULL;
    free(*n_list);
    *n_list = NULL; 
  }
  if(table != NULL){
    delete_Htable_and_content(table);
  }
}

/** ====================================================================== */
error_code network_get(client_t client, pps_key_t key, pps_value_t *value)
{
  size_t key_length = strlen(key);
  assert(key_length <= MAX_MSG_ELEM_SIZE);
  Htable_t table = construct_Htable(HTABLE_SIZE);
  error_code err = ERR_NOT_FOUND; /*return this if key is not found in Htable*/

  node_list_t* n_list = ring_get_nodes_for_key(client.ring, client.args->N, key);
  M_REQUIRE_NON_NULL_CUSTOM_ERR(n_list, ERR_NETWORK);

  char *buf = malloc(MAX_MSG_ELEM_SIZE + 1); // Extra '\0' when strcpy'n.
  // do we need to free buf afterwards?
  // yes! do this with network_del (see network.h) in every main
  M_REQUIRE_NON_NULL(buf);
  memset(buf, '\0', MAX_MSG_ELEM_SIZE + 1);

  int socket = get_socket((time_t)socket_time);

  for (size_t node_idx = 0; node_idx < client.args->N; ++node_idx) {
    node_t *node = n_list->list[node_idx];
    struct sockaddr *dest_addr = (struct sockaddr *)node->addr;
    ssize_t res = sendto(socket, key, key_length, 0, dest_addr,
                         sizeof(*dest_addr));
    M_CLOSURE_EXIT_IF(res < 0, network_end(&buf, socket, &n_list, &table),
                      ERR_NETWORK, "network_get", "sendto: %lu", res);
  }

  ssize_t res = -1;
  while(((res = recv(socket, buf, MAX_MSG_ELEM_SIZE, 0)) > 0) &&
          (buf[0] != '\0')){
    err = increase_count(&table, buf);
    M_CLOSURE_EXIT_IF_ERR(err, network_end(&buf, socket, &n_list, &table),
                          "network_get");
    if (verify_quorum(&table, buf, client.args->R) == 1) {
      *value = buf;
      network_end(NULL, socket, &n_list, &table);
      return ERR_NONE;
    }
  }

  network_end(&buf, socket, &n_list, &table);
  *value = NULL;
  return ERR_NOT_FOUND;
}

/** ====================================================================== */
error_code network_put(client_t client, pps_key_t key, pps_value_t value)
{
  size_t key_length = strlen(key);
  size_t val_length = strlen(value);
  assert(key_length <= MAX_MSG_ELEM_SIZE);
  assert(val_length <= MAX_MSG_ELEM_SIZE);
  //~ #ifdef DEBUG
    //~ print_ring(client.ring);
  //~ #endif
  node_list_t* n_list = ring_get_nodes_for_key(client.ring, client.args->N, key);
  #ifdef DEBUG
    print_ring(n_list);
  #endif
  M_REQUIRE_NON_NULL_CUSTOM_ERR(n_list, ERR_NETWORK);

  char buf[MAX_MSG_SIZE + 1]; // Extra '\0' when strcpy'n max params.
  strcpy(buf, key);
  strcpy(buf + key_length + 1, value);

  int socket = get_socket((time_t)socket_time);

  size_t payload_size = key_length + 1 + val_length;
  int quorum = false; /*will be true if successfully wrote in W servers*/
  int counter = 0;    /*count number of successful writes*/

  for (size_t node_idx = 0; node_idx < client.args->N; ++node_idx) {
    node_t *node = n_list->list[node_idx];

    struct sockaddr *dest_addr = (struct sockaddr *)node->addr;
    ssize_t res = sendto(socket, buf, payload_size, 0, dest_addr,
                         sizeof(*dest_addr));
    M_CLOSURE_EXIT_IF(res < 0, network_end(NULL, socket, &n_list, NULL),
                      ERR_NETWORK, "network_put", "sendto: %lu\nFAIL\n", res);
  }

  ssize_t res = -1;
  while((res = recv(socket, NULL, 0, 0)) == 0){
    if ((quorum == false) && (((size_t)(++counter)) >= client.args->W)) {
      quorum = true;
    }
  }
  network_end(NULL, socket, &n_list, NULL);
  
  return (quorum == true ? ERR_NONE : ERR_NETWORK);
}

/** ====================================================================== */
error_code network_del(client_t client, pps_key_t key);


