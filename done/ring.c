#include "ring.h"
#include "node_list.h"
#include "node.h"
#include "myutil.h"
#include "error.h"
#include <openssl/sha.h>

#define true 1
#define false 0
typedef int flag;

ring_t *ring_alloc()
{
  ring_t* ring = (ring_t*) node_list_new();
  M_FATAL_IF_NULL(ring, sizeof(ring_t), "ring_alloc");
  return ring;
}

error_code ring_init(ring_t *ring)
{
  M_EXIT_IF_ERR(get_nodes(ring), "ring_init");
  //sort ring according to SHA1
  node_list_sort(ring, node_cmp_sha);
  return ERR_NONE;
}


void ring_free(ring_t *ring)
{
  node_list_free(ring);
}


node_list_t *ring_get_nodes_for_key(const ring_t *ring, size_t wanted_list_size,
                                    pps_key_t key)
{
  node_list_t* n_list = node_list_new();
  
  /*allocate memory for one node first*/
  n_list->nb_nodes = 1;
  n_list->list = malloc(sizeof(node_t*));
  M_FATAL_CLOSURE_IF_NULL(n_list->list, free(n_list),
                         n_list->nb_nodes * sizeof(node_t *),
                        "Allocate node list");
  (void)memset(n_list->list, 0, sizeof(node_t*));

  /*calculate key SHA1*/
  unsigned char key_sha[SHA_DIGEST_LENGTH + 1];
  //strlen or strlen+1?
  char* key_aux = strdup(key);
  SHA1((unsigned char*)key_aux, strlen(key_aux), key_sha);   //md saves SHA code
  free(key_aux);
  #ifdef DEBUG
    //print_ring(ring);
    print_sha(key_sha);
  #endif

  //go through ring until the first_node after key_SHA
  flag found_first_node = false;
  size_t i;
  for(i = 0; (i < ring->nb_nodes) && (found_first_node == false); ++i){
    if(memcmp(key_sha, ring->list[i]->code_SHA, SHA_DIGEST_LENGTH) <= 0){
      found_first_node = true;
    }
  }
  if(found_first_node == false){
    /*
      /did not find first node/
      node_list_free(n_list); n_list = NULL;
      return NULL;
    */
    i = 1;
  }
  --i; //correcting last ++i to retrieve index of first node found
  
  n_list->list[0] = ring->list[i];
 
  //go around ring!
  flag already_added = false;
  size_t j = i+1;
  do{
    if(j >= ring->nb_nodes){
      j = 0; //go around ring;
    } 
    for(size_t k = 0; (k < n_list->nb_nodes) && (already_added == false); ++k){
      if(node_cmp_server_addr(&ring->list[j], &n_list->list[k]) == 0){
        already_added = true;
      }
    }
    if(already_added == false){
      error_code err = node_list_add(n_list, ring->list[j]);
      M_FATAL_CLOSURE_IF(err != ERR_NONE, node_list_free(n_list), err,
                        "ring_get_nodes_for_key",
                        "could not add node %zu of ring to list", j + 1);
    }
    already_added = false;
    ++j;
  }while((j != (i+1)) && (n_list->nb_nodes < wanted_list_size));

  if(n_list->nb_nodes < wanted_list_size){
    node_list_free(n_list); n_list = NULL;
    return NULL;
  }
  #ifdef DEBUG
    print_ring(n_list);
  #endif
  return n_list;
}
