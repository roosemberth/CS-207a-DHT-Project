/**
 * @file week05.c
 * @brief test code for pps-launch-server
 *
 * @author Roosembert Palacios
 * @date 01 Apr 2018
 */

#include <check.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "client.h"
#include "error.h"
#include "network.h"
#include "test-utils.h"

#define N_SERVERS 1
#define SERVER_BIN_PATH "./pps-launch-server"

pipeset pipes;

/**
 * Launches an instance of the server. Should be called at most
 * `N_SERVERS`-times.
 */
static void launch_server(uint16_t port) {
  size_t next_server_idx = pipes.next_idx;
  launch_program(&pipes, SERVER_BIN_PATH, NO_ARGS_ARR);

  expect_pipe(pipes, next_server_idx, 1, "IP port?\n");
  write_pipe(pipes, next_server_idx, 0, "127.0.0.1 %hu\n", port);
  expect_pipe(pipes, next_server_idx, 2, "Server listening on 127.0.0.1:%d.\n",
              port);
}

START_TEST(sending_value_to_server_and_retrieving_it_equals) {
  char **argvp = strtoargv("test-client -n 1 -w 1 -r 1");

  create_file("servers.txt", "127.0.0.1 1234\n");
  pipes = create_pipeset(N_SERVERS);
  launch_server(1234); // Launch server on localhost port 1234

  pps_key_t key = "42";
  pps_value_t expected =
      "Answer to the Ultimate Question of Life, the Universe, and Everything";
  pps_value_t actual;

  error_code err = ERR_NONE;

  client_t client;
  (void)memset(&client, 0, sizeof(client));
  client_init_args_t cli_init = {&client, 0, ALL_ARGS, 7, &argvp};
  if ((err = client_init(cli_init)) != ERR_NONE)
    ck_abort_msg("Client Init: %d", err);

  sleep(1);
  if ((err = network_put(client, key, expected)) != ERR_NONE)
    ck_abort_msg("Client Put: %d", err);
  if ((err = network_get(client, key, &actual)) != ERR_NONE)
    ck_abort_msg("Client Get: %d", err);
  client_end(cli_init.client);

  ck_assert_str_eq(expected, actual);

  kill_pipeset_programs(pipes);
  remove_file("servers.txt");
}
END_TEST

static Suite *suite(void) {
  Suite *s = suite_create("pps-client-*");

  TCase *tc_ht = tcase_create("Core");
  tcase_add_test(tc_ht, sending_value_to_server_and_retrieving_it_equals);

  suite_add_tcase(s, tc_ht);
  return s;
}

TEST_SUITE(suite)
