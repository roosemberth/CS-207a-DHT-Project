#include <stdio.h>
#include <stdlib.h>

#include "client.h"
#include "error.h"
#include "hashtable.h"
#include "myutil.h"
#include "network.h"
#include "util.h"

int main(int argc, char **argv) {
  client_t client;
  memset(&client, 0, sizeof(client));
  MAYBE_FAIL(client_init((client_init_args_t){&client, 4, ALL_ARGS,
                                              (size_t)argc, &argv}) != ERR_NONE,
             "pps-client-substr: client_init");

  pps_value_t value;
  int sol, len; // start of line, length

  M_EXIT_IF((sscanf(argv[1], "%d", &sol) != 1), ERR_IO, "pps-client-substr",
            "Get sol idx: %s", strerror(errno));
  M_EXIT_IF((sscanf(argv[2], "%d", &len) != 1), ERR_IO, "pps-client-substr",
            "Get length: %s", strerror(errno));

  MAYBE_FAIL(network_get(client, argv[0], &value) != ERR_NONE,
             "pps-client-substr: get");
  int value_length = (int)strlen(value);
  if (sol < 0)
    sol += value_length;

  MAYBE_FAIL(sol >= value_length, "Start of line out of range");
  MAYBE_FAIL(len > value_length - sol, "Not enough characters");
  MAYBE_FAIL(len < 0, "Length must be positive");

  char result[LINE_SIZE] = {0};

  memcpy(result, value + sol, (size_t)len);

  MAYBE_FAIL(network_put(client, argv[3], result) != ERR_NONE,
             "pps-client-substr: send");
  printf("OK\n");
  fflush(stdout);

  client_end(&client);
  return 0;
}
