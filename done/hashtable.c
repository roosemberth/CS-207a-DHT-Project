#include "hashtable.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "myutil.h"
#include "util.h"

#define true 1
#define false 0

/*
 * Definition of type for local hash-tables buckets
 */
struct bucket {
  kv_pair_t *kv_list;
  size_t kv_list_size;
};

// ======================================================================
void Htable_cpy_key_value(Htable_t *table, pps_key_t key, pps_value_t value,
                          size_t buckt_index, size_t kv_index) {
  table->buckets[buckt_index].kv_list[kv_index].key = (pps_key_t)strdup(key);
  table->buckets[buckt_index].kv_list[kv_index].value =
      (pps_value_t)strdup(value);
}

// ======================================================================
error_code add_Htable_value(Htable_t table, pps_key_t key, pps_value_t value) {
  error_code err = ERR_NONE;

  M_REQUIRE_NON_NULL(key);
  M_REQUIRE_NON_NULL(value);

  size_t index = hash_function(key, table.nb_buckets);
  M_EXIT_IF(index >= table.nb_buckets, ERR_BAD_PARAMETER, "add_Htable_value",
            "index = %zu greater than table.nb_buckets", index);

  /*Verify if bucket[index] is still empty and, if so, add value*/
  if ((table.buckets[index].kv_list_size == 1) &&
      (table.buckets[index].kv_list[0].key == NULL) &&
      (table.buckets[index].kv_list[0].value == NULL)) {
    // add key and value
    Htable_cpy_key_value(&table, key, value, index, 0);
  } else {
    int found_same_key = false;
    for (size_t i = 0; i < table.buckets[index].kv_list_size; ++i) {
      if (!strcmp(key, table.buckets[index].kv_list[i].key)) {
        /*overwrite value of key*/
        kv_pair_free(&(table.buckets[index].kv_list[i]));
        Htable_cpy_key_value(&table, key, value, index, i);
        found_same_key = true;
      }
    }

    if (!found_same_key) {
      /*Add kv_pair at end of bucket list*/
      ++table.buckets[index].kv_list_size;
      kv_pair_t *const old_kv_list = table.buckets[index].kv_list;
      if ((table.buckets[index].kv_list_size > SIZE_MAX / sizeof(kv_pair_t)) ||
          ((table.buckets[index].kv_list = realloc(
                table.buckets[index].kv_list,
                table.buckets[index].kv_list_size * sizeof(kv_pair_t))) ==
           NULL)) {
        table.buckets[index].kv_list = old_kv_list;
        --table.buckets[index].kv_list_size;
        M_EXIT(ERR_NOMEM,
               "Unable add extra kv pair to bucket[%zu].kv_list of size %zu",
               index, table.buckets[index].kv_list_size);
      } else {
        size_t kv_index = table.buckets[index].kv_list_size - 1;
        Htable_cpy_key_value(&table, key, value, index, kv_index);
      }
    }
  }
  return err;
}

// ======================================================================
pps_value_t get_Htable_value(Htable_t table, pps_key_t key) {
  if (key == NULL) {
    fprintf(stderr, "Sent NULL input to get_Htable_value\n");
    return NULL;
  }

  size_t index = hash_function(key, table.nb_buckets);
  if (index >= table.nb_buckets) {
    fprintf(stderr, "get_Htable_value: Parameter error\n");
    return NULL;
  }

  for (size_t i = 0; i < table.buckets[index].kv_list_size; ++i) {
    if (table.buckets[index].kv_list[i].key != NULL &&
        !strcmp(key, table.buckets[index].kv_list[i].key)) {
      /*get value*/
      return (table.buckets[index].kv_list[i].value);
    }
  }
  return NULL;
}

// ======================================================================
/** -------------------------------------------------------------------**
 * Hash a string for a given hashtable size.*
 * See http://en.wikipedia.org/wiki/Jenkins_hash_function
 **/
size_t hash_function(pps_key_t key, size_t size) {
  M_REQUIRE(size != 0, SIZE_MAX, "size == %d", 0);
  M_REQUIRE_NON_NULL_CUSTOM_ERR(key, SIZE_MAX);
  size_t hash = 0;
  const size_t key_len = strlen(key);
  for (size_t i = 0; i < key_len; ++i) {
    hash += (unsigned char)key[i];
    hash += (hash << 10);
    hash ^= (hash >> 6);
  }
  hash += (hash << 3);
  hash ^= (hash >> 11);
  hash += (hash << 15);

  return hash % size;
}

// ======================================================================
Htable_t construct_Htable(size_t size) {
  Htable_t table = {0}; // Initialize at zero.

  table.nb_buckets = size;
  table.buckets = calloc(size, sizeof(struct bucket));
  M_FATAL_IF(table.buckets == NULL, ERR_NOMEM, "construct_Htable",
             "hashtable of %zu buckets", size);

  /*Allocate memory for only one kv_pair in each bucket (for now)*/
  for (size_t i = 0; i < size; ++i) {
    table.buckets[i].kv_list = malloc(sizeof(kv_pair_t));
    M_FATAL_IF(table.buckets[i].kv_list == NULL, ERR_NOMEM, "construct_Htable",
               "table.buckets[%zu].kv_list", i);
    (void)memset(table.buckets[i].kv_list, 0, sizeof(kv_pair_t));
    table.buckets[i].kv_list_size = 1;
  }
  return table;
}

// ======================================================================
void delete_Htable_and_content(Htable_t *table) {
  if (table != NULL) {
    if (table->buckets != NULL) {
      for (size_t i = 0; i < table->nb_buckets; ++i) {
        delete_bucket(&(table->buckets[i]));
      }
      free(table->buckets);
      table->buckets = NULL;
    }
    table->nb_buckets = 0;
  }
}

// ======================================================================
void delete_bucket(struct bucket *buckt) {
  if (buckt != NULL) {
    if (buckt->kv_list != NULL) {
      for (size_t i = 0; i < buckt->kv_list_size; ++i) {
        kv_pair_free(&(buckt->kv_list[i]));
      }
      free(buckt->kv_list);
      buckt->kv_list = NULL;
    }
    buckt->kv_list_size = 0;
  }
}

// ======================================================================
void kv_pair_free(kv_pair_t *kv) {
  if (kv != NULL) {
    if (kv->key != NULL) {
      my_free_const_ptr(kv->key);
    }
    if (kv->value != NULL) {
      my_free_const_ptr(kv->value);
    }
  }
}

// ======================================================================
kv_list_t *dump_hashtable(Htable_t *table) {
  kv_list_t *head = malloc(sizeof(kv_list_t));
  kv_list_t *tail = head;
  for (size_t bn = 0; bn < table->nb_buckets; ++bn) {
    for (size_t kvi = 0; kvi < table->buckets[bn].kv_list_size; ++kvi) {
      tail->next = malloc(sizeof(kv_list_t));
      tail = tail->next;
      tail->pair = &table->buckets[bn].kv_list[kvi];
      tail->next = NULL;
    }
  }
  tail = head->next;
  free(head);
  return tail;
}

void free_kv_pair_list(kv_list_t *list) {
  if (list == NULL)
    return;
  free_kv_pair_list(list->next);
  free(list);
}
