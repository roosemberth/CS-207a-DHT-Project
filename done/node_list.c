#include "node_list.h"
#include "config.h"
#include "error.h"
#include "myutil.h"
#include "node.h"
#include "util.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define true 1
#define false 0

// ======================================================================
node_list_t *node_list_new(void) {
  node_list_t *n_list = malloc(sizeof(node_list_t));
  M_FATAL_IF_NULL(n_list, sizeof(node_list_t), "node_list_new");
  (void)memset(n_list, 0, sizeof(node_list_t)); /*initialize memory*/
  return n_list;
}

// ======================================================================
static node_list_t* enlarge(node_list_t* n_list, size_t size){
  node_list_t* result = n_list;
  if((result != NULL) && (result->list != NULL)){
    node_t** const old_list = result->list;
    result->nb_nodes += size;
    if((result->nb_nodes > SIZE_MAX/sizeof(node_t*)) ||
        ((result->list = realloc(result->list,
                                result->nb_nodes*sizeof(node_t*)))
          == NULL)){
      result->list = old_list;
      result->nb_nodes -= size;
      result = NULL;
      return result;
    }
    for(size_t idx = (result->nb_nodes-size); idx < result->nb_nodes; ++idx){
      result->list[idx] = malloc(sizeof(node_t));
      if(result->list[idx] == NULL){
        result = NULL;
        return result;
      }
      (void)memset(result->list[idx], 0, sizeof(node_t)); /*initialize memory*/
    }
  }
  return result;
}

// ======================================================================
error_code get_nodes(node_list_t* n_list) {
  
  FILE *fp = open_file_readonly(PPS_SERVERS_LIST_FILENAME);
  M_CLOSURE_EXIT_IF_NULL(fp, free(n_list), sizeof(FILE *), "Open server list.");

  size_t nb_lines = (size_t) count_file(fp).nb_lines;
  /*start by allocating memory for one node, for each server*/
  n_list->nb_nodes = nb_lines;

  /*allocate memory for list of nodes of size nb_nodes*/
  //changed from malloc to calloc
  n_list->list = calloc(n_list->nb_nodes, sizeof(node_t *));
  M_CLOSURE_EXIT_IF_NULL(n_list->list, free(n_list),
                         n_list->nb_nodes * sizeof(node_t *),
                         "Allocate node list");

  for (size_t idx = 0; idx < n_list->nb_nodes; ++idx) {
    n_list->list[idx] = malloc(sizeof(node_t));
    M_CLOSURE_EXIT_IF_NULL(n_list->list[idx], node_list_free(n_list),
                           sizeof(node_t), "Create nodes");
    (void)memset(n_list->list[idx], 0, sizeof(node_t)); /*initialize memory*/
  }

  rewind(fp); /*rewind to beggining of file*/
  char line[LINE_SIZE];
  char ip[IP_SIZE + 1];
  uint16_t port = 0;
  size_t tmp_nb_nodes = 0;
  int aux_num_nodes = 0; 
  error_code err = ERR_NONE;

  for (size_t i = 0; i < nb_lines; ++i) {
    get_str_file(line, LINE_SIZE, fp);
    port = 0;
    aux_num_nodes = 0;
    err = read_ip_port_id(line, ip, IP_SIZE, &port, &aux_num_nodes,
                        max_PORT) == 0
              ? ERR_NONE
              : ERR_BAD_PARAMETER;
    M_FATAL_CLOSURE_IF(err != ERR_NONE, node_list_free(n_list), err,
                       "get_nodes",
                       "Invalid ipaddr/port/nb_nodes specification in line %lu", i + 1);

    size_t server_num_nodes = (size_t)aux_num_nodes;
    /* enlarge node_list by server_num_nodes-1, if server_num_nodes > 1
     * (memory for 1 node already allocated)*/
    if(server_num_nodes > 1){
      M_CLOSURE_EXIT_IF_NULL(enlarge(n_list, (server_num_nodes-1)),
                              node_list_free(n_list),
                              n_list->nb_nodes * sizeof(node_t*),
                              "Allocate node list");
    }

    for(size_t j = 0; j < server_num_nodes; ++j){
      err = node_init(n_list->list[tmp_nb_nodes + j], ip, port, j + 1);
      M_CLOSURE_EXIT_IF(err != ERR_NONE, node_list_free(n_list), err,
                         "get_nodes", "Initializing %lu node",
                          tmp_nb_nodes + j);      
    }
    tmp_nb_nodes += server_num_nodes;
  }
  
  if(n_list->nb_nodes != tmp_nb_nodes){
    fprintf(stderr, "something went wrong in get_nodes\n");
  }

  fclose(fp);
  return ERR_NONE;
}

// ======================================================================
void node_list_free(node_list_t *n_list) {
  if (n_list != NULL) {
    if (n_list->list != NULL) {
      for (size_t i = 0; i < n_list->nb_nodes; i++) {
        if (n_list->list[i] != NULL) {
          node_end(n_list->list[i]);
          free(n_list->list[i]);
          n_list->list[i] = NULL;
        }
      }
      free(n_list->list);
      n_list->list = NULL;
    }
    n_list->nb_nodes = 0;
    free(n_list);
  }
}

// ======================================================================
error_code node_list_add(node_list_t *n_list, node_t* node) {
  M_REQUIRE_NON_NULL(n_list);
  M_REQUIRE_NON_NULL(n_list->list);

  error_code err = ERR_NONE;
  ++n_list->nb_nodes;
  
  node_t **const old_n_list = n_list->list;
  if ((n_list->nb_nodes > SIZE_MAX / sizeof(node_t *)) ||
      ((n_list->list = realloc(n_list->list,
                               n_list->nb_nodes * sizeof(node_t *))) == NULL)) {
    n_list->list = old_n_list;
    --n_list->nb_nodes;
    M_EXIT(ERR_NOMEM, "Unable add extra node to list of size %zu",
           n_list->nb_nodes);
  } else {
    n_list->list[n_list->nb_nodes - 1] = node;
  }

  return err;
}

// ======================================================================
void node_list_sort(node_list_t *n_list,
                    int (*comparator)(const void*, const void*))
{
  qsort(n_list->list, n_list->nb_nodes, sizeof(node_t*), comparator);
}
