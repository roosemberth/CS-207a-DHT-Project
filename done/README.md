What we have done:
We have done all the work, except the report with the graph of the traffic analysis, since we had to spend some time correcting memory leaks and corruptions that were discovered through pps-client-benchmark.

Estimation of time:
10 hours/person
This course is very good, but the workload is absolutely disconnected from the number of credits. The project should be a 5 or 6 credit course.
In any case, the course content was really good.
