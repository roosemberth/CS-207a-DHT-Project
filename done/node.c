#include <stdlib.h>
#include <string.h>
#include <openssl/sha.h>
#include <math.h>

#include "myutil.h"
#include "node.h"
#include "system.h" 

error_code node_init(node_t *node, const char *ip, uint16_t port,
                     size_t _unused node_id)
{
  error_code err = ERR_NONE;
  node->addr = malloc(sizeof(struct sockaddr_in));
  /*specify node_id in the next error call, for the following weeks*/
  M_EXIT_IF_NULL(node->addr, (int)sizeof(struct sockaddr_in), "node_init");

  node->node_id = node_id;

  size_t str_size = max_ip_port_size + max_node_id_size + 2;
  char ip_port_id[str_size];
  memset(ip_port_id, 0, str_size);
  int length = snprintf(ip_port_id, str_size, "%s %hu %zu", ip, port, node_id);

  //length or length+1?
  SHA1((unsigned char*)ip_port_id, (size_t)length, node->code_SHA);   //md saves SHA code

  (void)memset(node->addr, 0, sizeof(struct sockaddr_in));
  err = get_server_addr(ip, port, node->addr);
  
  return err;
}

// ======================================================================
void node_end(node_t *node)
{
  if((node != NULL) && (node->addr != NULL)){
    free(node->addr);
    node->addr = NULL;
  }
}

// ======================================================================
int node_cmp_sha(void const* arg1, void const* arg2)
{
  node_t const * const * const first = arg1;
  node_t const * const * const second = arg2;

  /*print_sha(first->code_SHA);
  print_sha(second->code_SHA);
  int i = memcmp(first->code_SHA, second->code_SHA, SHA_DIGEST_LENGTH);*/
  return (memcmp((*first)->code_SHA, (*second)->code_SHA, SHA_DIGEST_LENGTH));
}

// ======================================================================
int node_cmp_server_addr(void const* arg1, void const* arg2)
{
  M_REQUIRE_NON_NULL(arg1);
  M_REQUIRE_NON_NULL(arg2);

  node_t const * const * const first = arg1;
  node_t const * const * const second = arg2;
  
  size_t str_size = max_ip_port_size;
  char first_ip_port[str_size];
  memset(first_ip_port, 0, str_size);
  char second_ip_port[str_size];
  memset(second_ip_port, 0, str_size);

  ip_port_str_from_node(*first, first_ip_port, str_size);
  ip_port_str_from_node(*second, second_ip_port, str_size);

  return(strcmp(first_ip_port, second_ip_port));
}


