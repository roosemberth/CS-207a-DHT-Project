/**
 * @file myutil.c
 * @brief Some tool functions
 *
 */

#include <ctype.h> // for isspace
#include <errno.h>
#include "error.h"
#include <inttypes.h>
#include <stdbool.h> // for flags in count function
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h> // for sockets
#include <netinet/in.h> // for IPPROTO_UDP
#include <arpa/inet.h> // for inet_pton
#include "myutil.h"

// ======================================================================
error_code get_str_file(char *response, int size, FILE *fp) {
  size_t used_size = 0;

  if (fgets(response, size, fp) == NULL){
    return ERR_IO;
  }
  used_size = strlen(response) - 1;
  if (response[used_size] == '\n') {
    response[used_size] = '\0';
  }

  if (used_size > 0)
    return ERR_NONE;
  return ERR_IO;
}

// ======================================================================
error_code askfor_int(int *i_ptr) {
  return scanf("%d\n", i_ptr) == 1 ? ERR_NONE : ERR_IO;
}

// ======================================================================
counts count_file(FILE *f) {
  counts thisCount;
  (void)memset(&thisCount, 0, sizeof(thisCount));
  unsigned long int nb_lines = 0;
  unsigned long int nb_words = 0;
  unsigned long int nb_chars = 0;
  int c = 0;
  bool BlankFlag = true;        /* signals Blank spaces  */
  bool SeenFirstLetter = false; /* signals whether we have already
                           seen first letter or not, to treat
                           cases where file starts with Blanks */
  bool lastCharWasNewLine = false;

  while ((c = fgetc(f)) != EOF) {
    if (lastCharWasNewLine)
      lastCharWasNewLine = false;
    if (isspace(c)) {
      if (c == (int)'\n') {
        lastCharWasNewLine = true;
        // increment line
        ++nb_lines;
      }
      if (BlankFlag == false) {
        // signal blank space
        BlankFlag = true;
      }
    } else if (BlankFlag == true) {
      // first character found after space(s)
      BlankFlag = false;
      if (SeenFirstLetter == true) {
        // count last word
        ++nb_words;
      } else {
        // there was no word before in file
        SeenFirstLetter = true;
      }
    }

    // increment number of chars
    ++nb_chars;
  }

  ++nb_words; // add last word
  if (!lastCharWasNewLine)
    ++nb_lines; // add line before EOF (no \n)

  thisCount.nb_lines = nb_lines;
  thisCount.nb_words = nb_words;
  thisCount.nb_chars = nb_chars;

  return thisCount;
}

// ======================================================================
FILE *open_file_readonly(const char *filename) {
  if (filename == NULL) {
    return NULL;
  }
  FILE *file = fopen(filename, "r");
  if (file == NULL) {
    fprintf(stderr, "ERROR: cannot open file \"%s\"\n", filename);
  }
  return file;
}

// ======================================================================
int read_ip_port(const char *line, char *ip, size_t SIZE_ip, uint16_t *port,
                 const int MAX_port) {
  int i = 0;
  char aux_ip[strlen(line) + 1];
  i = sscanf(line, "%s %hu", aux_ip, port);

  if ((i == 2) && ((*port) <= MAX_port) && (strlen(aux_ip) <= SIZE_ip)) {
    strncpy(ip, aux_ip, strlen(aux_ip) + 1);
    return 0;
  } else {
    return 1;
  }
}

// ======================================================================
int read_ip_port_id(const char *line, char *ip, size_t SIZE_ip, uint16_t *port,
                 int* num_nodes, const int MAX_port) {
  int i = 0;
  char aux_ip[strlen(line) + 1];
  i = sscanf(line, "%s %hu %d", aux_ip, port, num_nodes);

  if ((i == 3) && ((*port) <= MAX_port) && (strlen(aux_ip) <= SIZE_ip) &&
       ((*num_nodes) >= 1)) {
    strncpy(ip, aux_ip, strlen(aux_ip) + 1);
    return 0;
  } else {
    return 1;
  }
}

// ======================================================================
int ask_ip_port(char *pps_ip, size_t ip_size, uint16_t *pps_port) {
  int r;
  uint8_t ip1, ip2, ip3, ip4;
  do {
    printf("IP port?\n");
    fflush(stdout);
    r = scanf("%hhu.%hhu.%hhu.%hhu %hu", &ip1, &ip2, &ip3, &ip4, pps_port);
    if (errno != 0)
      perror("fgets");
  } while (r != 5 && !feof(stdin) && !ferror(stdin));

  r = snprintf(pps_ip, ip_size, "%hhu.%hhu.%hhu.%hhu", ip1, ip2, ip3, ip4);
  if (r < 0 || ip_size < (unsigned)r) {
    fprintf(stderr, "Could not write ip address response into buffer.");
    return 1;
  }
  return 0;
}

// ======================================================================
int cmd_line_ip_port(char **const argv, char *pps_ip, size_t ip_size,
                     uint16_t *pps_port) {
  int r;
  int k;
  uint8_t ip1, ip2, ip3, ip4;

  r = sscanf(argv[0], "%hhu.%hhu.%hhu.%hhu", &ip1, &ip2, &ip3, &ip4);
  if (errno != 0)
    perror("fgets");
  k = sscanf(argv[1], "%hu", pps_port);
  if (errno != 0)
    perror("fgets");
  if ((r != 4) || (k != 1)) {
    fprintf(stderr, "could not get ip port from command line\n");
    return 1;
  }

  r = snprintf(pps_ip, ip_size, "%hhu.%hhu.%hhu.%hhu", ip1, ip2, ip3, ip4);
  if (r < 0 || ip_size < (unsigned)r) {
    fprintf(stderr, "Could not write ip address response into buffer.");
    return 1;
  }
  return 0;
}

// ======================================================================
void ip_port_str_from_node(const node_t *node, char* str_ip_port,
                            size_t str_size){
  if(node != NULL){
    char node_ip[IP_SIZE+1] = {0};
    uint16_t node_port = 0;
    
    inet_ntop(AF_INET, &node->addr->sin_addr, node_ip, IP_SIZE);
    node_port = ntohs(node->addr->sin_port);
    snprintf(str_ip_port, str_size, "%s %hu", node_ip, node_port);    
  }
}

// ======================================================================
void print_sha(const unsigned char* code_SHA)
{
  if(code_SHA == NULL) {
    fprintf(stderr, "Error in code SHA\n");
    return;
  } else {
    printf(" ("); fflush(stdout);
    for (size_t i = 0; i < SHA_DIGEST_LENGTH ; i++) {
      printf("%02x", code_SHA[i]); fflush(stdout);
    }
    printf(") "); fflush(stdout);
  }
}

// ======================================================================
void print_ring(const ring_t* ring){

  size_t str_size = max_ip_port_size;
  char ip_port[str_size];
  memset(ip_port, 0, str_size);

  for(size_t idx = 0; idx < ring->nb_nodes; ++idx){
    ip_port_str_from_node(ring->list[idx], ip_port, str_size);
    printf("%s", ip_port); fflush(stdout);
    print_sha(ring->list[idx]->code_SHA);
    putchar('\n'); fflush(stdout);
  }
  putchar('\n'); fflush(stdout);
}

// ======================================================================
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-qual"
void my_free_const_ptr(const void *x) { free((void *)x); }
#pragma GCC diagnostic pop
