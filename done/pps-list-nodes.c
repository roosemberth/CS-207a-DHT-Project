#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define DEBUG 1
#include "config.h"
#include "error.h"
#include "myutil.h"
#include "ring.h"
#include "node.h"
#include "node_list.h"
#include "system.h"
#include "util.h"


typedef struct{
  size_t* num_nodes;   //number of nodes for each server (position on vector)
  size_t num_servers;
  int* sockets;        //one socket for each server communication to distinguish them
  char (*str_arr)[max_ip_port_size + 1];
} file_read;

static void free_file_read(file_read* read)
{
  if(read->num_nodes != NULL){
    free(read->num_nodes);
    read->num_nodes = NULL;
  }
  if(read->sockets != NULL){
    for(size_t i = 0; i < read->num_servers; ++i){
      close(read->sockets[i]);
    }
    free(read->sockets);
    read->sockets = NULL;
  }
  if(read->str_arr != NULL){
    free(read->str_arr);
    read->str_arr = NULL;    
  }
}

static void end(FILE* fp, ring_t* ring, file_read* read)
{
  if(fp != NULL){
    fclose(fp);    
  }
  if(ring != NULL){
    ring_free(ring);    
  }
  if(read != NULL){
    free_file_read(read);    
  }
}


int main(int argc, char **argv) {

  M_FATAL_IF(argc != 1, ERR_BAD_PARAMETER, "pps-list-node",
             "did not respect %d mandatory arguments", 2);

  ring_t* ring = ring_alloc();
  M_FATAL_IF(ring_init(ring) != ERR_NONE, ERR_NETWORK,
                    "pps-list-nodes", "Unable to Initialize eing: %s",
                    strerror(errno));

  //sort according to server address
  qsort(ring->list, ring->nb_nodes, sizeof(node_t*), node_cmp_server_addr);
  #ifdef DEBUG
    print_ring(ring);
  #endif

  FILE *fp = open_file_readonly(PPS_SERVERS_LIST_FILENAME);
  M_CLOSURE_EXIT_IF(fp == NULL, ring_free(ring), ERR_IO,
                    "pps_list_nodes", "Opening servers file %s",
                    PPS_SERVERS_LIST_FILENAME);
                    
  error_code err = ERR_NONE;
  node_t *node = NULL;

  // This is a mock message, since length will be zero
  char buf_send[] = "\0";

  size_t num_servers = (size_t) count_file(fp).nb_lines;
  rewind(fp); /*rewind to beggining of file*/

  file_read read; /* structure for saving number of servers and number 
                   * of nodes for each server */
                   
  read.num_servers = 0; //will be equal to num_servers at end, if successful
  
  read.num_nodes = calloc(num_servers, sizeof(size_t));
  M_CLOSURE_EXIT_IF_NULL(read.num_nodes, end(fp, ring, NULL),
                        num_servers * sizeof(size_t), "file_read struct");
                        
  read.str_arr = calloc(num_servers, max_ip_port_size + 1);
  M_CLOSURE_EXIT_IF_NULL(read.str_arr, end(fp, ring, &read),
                  sizeof(char)*(max_ip_port_size+1), "file_read struct");

  read.sockets = calloc(num_servers, sizeof(int));
  M_CLOSURE_EXIT_IF_NULL(read.sockets, end(fp, ring, &read),
                  sizeof(char)*(max_ip_port_size+1), "file_read struct");

  for(size_t i = 0; i < num_servers; ++i){
    // if server does not reply within timeout, print FAIL:
    read.sockets[i] = get_socket((time_t)1); // timeout = 1 second
  }
                 
  char line[LINE_SIZE];
  size_t node_idx = 0;

  while(get_str_file(line, LINE_SIZE, fp) == ERR_NONE){
    
    int aux_num_nodes = 0;
    char ip[IP_SIZE + 1];
    uint16_t port = 0;
    
    err = read_ip_port_id(line, ip, IP_SIZE, &port, &aux_num_nodes, max_PORT) == 0
              ? ERR_NONE
              : ERR_BAD_PARAMETER;
    M_CLOSURE_EXIT_IF(err != ERR_NONE, end(fp, ring, &read), err, "cfg node",
                      "Invalid ipaddr/port spec in line %lu", node_idx + 1);
    size_t server_num_nodes = (size_t)aux_num_nodes;
    
    node = ring->list[node_idx];
    struct sockaddr *dest_addr = (struct sockaddr *)node->addr;
    ssize_t res = sendto(read.sockets[read.num_servers], buf_send, 0, 0,
                          dest_addr, sizeof(*dest_addr));
    M_CLOSURE_EXIT_IF(res < 0, end(fp, ring, &read), ERR_NETWORK, "ping node",
                      "Error sending to node %lu: %s", node_idx + 1,
                      strerror(errno));

    node_idx += server_num_nodes;
    
    read.num_nodes[read.num_servers] = server_num_nodes;
    snprintf(read.str_arr[read.num_servers], max_ip_port_size, "%s %hu ", ip, port);
    read.str_arr[read.num_servers][max_ip_port_size] = '\0'; 
    ++read.num_servers;
  }

  // Initialize received message buffer:
  char buf_rcv[MAX_MSG_ELEM_SIZE + 1];
  memset(buf_rcv, '\0', MAX_MSG_ELEM_SIZE + 1);
  node_idx = 0;
  ssize_t res = -1;
  
  for(size_t i = 0; i < read.num_servers; ++i){
    res = recv(read.sockets[i], buf_rcv, MAX_MSG_ELEM_SIZE, 0);
    for(size_t j = node_idx; j < (node_idx + read.num_nodes[i]); ++j){
      printf("%s", read.str_arr[i]);
      print_sha(ring->list[j]->code_SHA);
      if (res == 0) {
        printf("OK\n");
      } else if (res < 0) {
        printf("FAIL\n");
      }
    }
    node_idx += read.num_nodes[i];
  }

  end(fp, ring, &read);
  return 0;
}
