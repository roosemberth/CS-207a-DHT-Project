#pragma once

#include <stdlib.h> // EXIT_FAILURE

#define TEST_SUITE(get_suite)                                                  \
  int main(void) {                                                             \
    SRunner *sr = srunner_create(get_suite());                                 \
    srunner_run_all(sr, CK_VERBOSE);                                           \
                                                                               \
    int number_failed = srunner_ntests_failed(sr);                             \
    srunner_free(sr);                                                          \
                                                                               \
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;                 \
  }

typedef struct {
  size_t n_pipes;
  int **fd;   // parent: [i][stdin, stdout, stderr]
  int **ch;   // child: [i][stdin, stdout, stderr]
  FILE ***fs; // parent: [i][stdin, stdout, stderr] FILE *
  size_t next_idx;
  pid_t *pids;
} pipeset;

pipeset create_pipeset(size_t n_pipes);

void run_program(size_t program_idx, pipeset *pipes, const char *path,
                    char *const argv[]);
void launch_program(pipeset *pipes, const char *path, char *const argv[]);

void kill_pipeset_programs(pipeset pipes);

void write_pipe(const pipeset pipes, size_t pipe_idx, size_t stream_idx,
                const char *fmt, ...);
void expect_pipe(const pipeset pipes, size_t pipe_idx, size_t stream_idx,
                 const char *expected_fmt, ...);

void create_file(const char *name, const char *contents);
void remove_file(const char *name);

char **strtoargv(const char *str);

/**
 * Closes and clears a pipe.
 *
 * eg. TERMINATE_PIPE(pipes.fs[idx][0])
 */
#define TERMINATE_PIPE(p)                                                      \
  if (p != NULL)                                                               \
    do {                                                                       \
      fclose(p);                                                               \
      p = NULL;                                                                \
  } while (0)

#define NO_ARGS_ARR ((char *[]){(char *)NULL})
