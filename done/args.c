#include "args.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define true 1
#define false 0
typedef int flag;
typedef enum { N, W, R, NONE } Option;

args_t *parse_opt_args(size_t supported_args, char ***rem_argv) {
  args_t *args = malloc(sizeof(args_t));
  if (args == NULL) {
    fprintf(stderr, "error in args_t memory allocation\n");
    return NULL;
  }

  // default values:
  args->N = 3;
  args->R = 2;
  args->W = 2;

  Option option = NONE;
  flag new_option = true;

  while ((*rem_argv)[0] != NULL) {
    if (new_option == true) {
      if (strcmp((*rem_argv)[0], "-n") == 0) {
        if (supported_args & TOTAL_SERVERS) {
          option = N;
        } else {
          fprintf(stderr, "Invalid option -n\n");
          free(args);
          args = NULL;
          return args;
        }
      } else if (strcmp((*rem_argv)[0], "-w") == 0) {
        if (supported_args & PUT_NEEDED) {
          option = W;
        } else {
          fprintf(stderr, "Invalid option -w\n");
          free(args);
          args = NULL;
          return args;
        }
      } else if (strcmp((*rem_argv)[0], "-r") == 0) {
        if (supported_args & GET_NEEDED) {
          option = R;
        } else {
          fprintf(stderr, "Invalid option -r\n");
          free(args);
          args = NULL;
          return args;
        }
      } else if (strcmp((*rem_argv)[0], "--") == 0) {
        ++(*rem_argv);
        return args;
      } else {
        return args;
      }
      new_option = false;
    } else {
      int i;
      int j;
      j = sscanf((*(rem_argv)[0]), "%d", &i);
      if ((j != 1) || (i <= 0)) {
        fprintf(stderr, "value for one of the options is invalid\n");
        free(args);
        args = NULL;
        return args;
      } else {
        switch (option) {
        case N:
          args->N = (size_t)i;
          break;
        case W:
          args->W = (size_t)i;
          break;
        case R:
          args->R = (size_t)i;
          break;
        default:
          fprintf(stderr, "Invalid option?\n");
          free(args);
          args = NULL;
          return args;
        }
      }
      new_option = true;
    }
    // next argument
    ++(*rem_argv);
  }

  if (new_option == false) {
    // all arguments were seen and missing value for option
    free(args);
    args = NULL;
    return args;
  }
  // all arguments were seen
  return args;
}
