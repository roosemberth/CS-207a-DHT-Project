#define _POSIX_C_SOURCE 199309L
#include <inttypes.h>
#include <stdio.h>

#include "client.h"
#include "error.h"
#include "hashtable.h"
#include "myutil.h"
#include "network.h"
#include "node.h"
#include "system.h"
#include "util.h"


static void print_time(const struct timespec* p_time_start,
                      const struct timespec* p_time_end)
{
  long nsec = p_time_end->tv_nsec - p_time_start->tv_nsec;
  while(nsec < 0) nsec += 1000000000;
  printf("%ld%09ld\n", p_time_end->tv_sec - p_time_start->tv_sec, nsec);
}

int main(void) {
  
  char name[] = "pps-client-put";
  
#define num_args 1  //only name
  int argc = num_args;

  for(size_t n = 1; n < 101; n+=10){
    
    char* argv[] = {name};
    char** aux = argv;
    char*** argv_ptr = &aux;
    
    client_t client;
    memset(&client, 0, sizeof(client));
    MAYBE_FAIL(
        client_init((client_init_args_t){&client, 0, TOTAL_SERVERS | PUT_NEEDED,
                                         (size_t)argc, argv_ptr}) != ERR_NONE,
        "pps-client-put: client_init");
        
    client.args->N = n;
    client.args->W = n/2+1;
    client.args->R = n/2+1;
    
    struct timespec time_start, time_end;
    int time_start_ret = clock_gettime(CLOCK_MONOTONIC, &time_start);
    M_EXIT_IF_ERR(time_start_ret, "clock error");

    MAYBE_FAIL(network_put(client, "a", "beta") != ERR_NONE,
               "pps-client-put: send");

    printf("OK\n");
    fflush(stdout);
    
    int time_end_ret = clock_gettime(CLOCK_MONOTONIC, &time_end);
    M_EXIT_IF_ERR(time_end_ret, "clock error");

    print_time(&time_start, &time_end);

    client_end(&client);
  }
  return 0;
}
