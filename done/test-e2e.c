/**
 * @file week05.c
 * @brief test code for pps-launch-server
 *
 * @author Roosembert Palacios
 * @date 01 Apr 2018
 */

#include <check.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "client.h"
#include "error.h"
#include "network.h"
#include "test-utils.h"

#define SERVER_BIN_PATH "./pps-launch-server"
#define PPS_CLIENT_CAT_BIN_PATH "./pps-client-cat"
#define PPS_CLIENT_FIND_BIN_PATH "./pps-client-find"
#define PPS_CLIENT_GET_BIN_PATH "./pps-client-get"
#define PPS_CLIENT_PUT_BIN_PATH "./pps-client-put"
#define PPS_CLIENT_SUBSTR_BIN_PATH "./pps-client-substr"
#define PPS_DUMP_NODE_BIN_PATH "./pps-dump-node"
#define ARG_LINE_SIZE 1024

#define ADD_AND_CHECK(target, value)                                           \
  do {                                                                         \
    char args[ARG_LINE_SIZE];                                                  \
    snprintf(args, ARG_LINE_SIZE,                                              \
             PPS_CLIENT_PUT_BIN_PATH " -n 1 -w 1 -- %s %s", target, value);    \
    run_program(1, &pipes, PPS_CLIENT_PUT_BIN_PATH, strtoargv(args));          \
    expect_pipe(pipes, 1, 1, "OK\n");                                          \
    snprintf(args, ARG_LINE_SIZE, PPS_CLIENT_GET_BIN_PATH " -n 1 -r 1 -- %s",  \
             target);                                                          \
    run_program(1, &pipes, PPS_CLIENT_GET_BIN_PATH, strtoargv(args));          \
    expect_pipe(pipes, 2, 1, "OK " value "\n");                                \
  } while (0)

START_TEST(week07_handout_tests) {
  create_file("servers.txt", "127.0.0.1 1234\n");
  pipeset pipes = create_pipeset(3);
  launch_program(&pipes, SERVER_BIN_PATH, NO_ARGS_ARR);

  expect_pipe(pipes, 0, 1, "IP port?\n");
  write_pipe(pipes, 0, 0, "127.0.0.1 1234\n");
  expect_pipe(pipes, 0, 2, "Server listening on 127.0.0.1:1234.\n");

  launch_program(&pipes, PPS_CLIENT_PUT_BIN_PATH, strtoargv("put -n 1 -w 1"));
  launch_program(&pipes, PPS_CLIENT_GET_BIN_PATH, strtoargv("get -n 1 -r 1"));

  ADD_AND_CHECK("abc", "xy");

  write_pipe(pipes, 2, 0, "something\n");
  expect_pipe(pipes, 2, 1, "FAIL\n");

  write_pipe(pipes, 1, 0, "something\n");
  write_pipe(pipes, 1, 0, "some_value\n");
  expect_pipe(pipes, 1, 1, "OK\n");

  write_pipe(pipes, 1, 0, "u\n");
  write_pipe(pipes, 1, 0, "v\n");
  expect_pipe(pipes, 1, 1, "OK\n");

  write_pipe(pipes, 2, 0, "something\n");
  expect_pipe(pipes, 2, 1, "OK some_value\n");

  write_pipe(pipes, 2, 0, "u\n");
  expect_pipe(pipes, 2, 1, "OK v\n");

  write_pipe(pipes, 2, 0, "v\n");
  expect_pipe(pipes, 2, 1, "FAIL\n");

  kill_pipeset_programs(pipes);
  remove_file("servers.txt");
}
END_TEST

START_TEST(pps_dump_node) {
  create_file("servers.txt", "127.0.0.1 1234\n");
  pipeset pipes = create_pipeset(4);
  launch_program(&pipes, SERVER_BIN_PATH, NO_ARGS_ARR);

  expect_pipe(pipes, 0, 1, "IP port?\n");
  write_pipe(pipes, 0, 0, "127.0.0.1 1234\n");
  expect_pipe(pipes, 0, 2, "Server listening on 127.0.0.1:1234.\n");

  launch_program(&pipes, PPS_CLIENT_PUT_BIN_PATH, strtoargv("put -n 1 -w 1"));
  launch_program(&pipes, PPS_CLIENT_GET_BIN_PATH, strtoargv("get -n 1 -r 1"));
  launch_program(&pipes, PPS_DUMP_NODE_BIN_PATH,
                 strtoargv("dump 127.0.0.1 1234"));

  ADD_AND_CHECK("abc", "xy");
  ADD_AND_CHECK("dsad", "ds");
  ADD_AND_CHECK("ssd", "1995");

  expect_pipe(pipes, 3, 1, "IP port?\n");
  write_pipe(pipes, 3, 0, "127.0.0.1 1234\n");
  expect_pipe(pipes, 3, 1, "abc = xy\n");
  expect_pipe(pipes, 3, 1, "dsad = ds\n");
  expect_pipe(pipes, 3, 1, "ssd = 1995\n");

  kill_pipeset_programs(pipes);
  remove_file("servers.txt");
}
END_TEST

START_TEST(pps_dump_node_stress_test) {
  create_file("servers.txt", "127.0.0.1 1234\n");
  pipeset pipes = create_pipeset(4);
  launch_program(&pipes, SERVER_BIN_PATH, NO_ARGS_ARR);

  expect_pipe(pipes, 0, 1, "IP port?\n");
  write_pipe(pipes, 0, 0, "127.0.0.1 1234\n");
  expect_pipe(pipes, 0, 2, "Server listening on 127.0.0.1:1234.\n");

  launch_program(&pipes, PPS_CLIENT_PUT_BIN_PATH, strtoargv("put -n 1 -w 1"));
  launch_program(&pipes, PPS_CLIENT_GET_BIN_PATH, strtoargv("get -n 1 -r 1"));
  launch_program(&pipes, PPS_DUMP_NODE_BIN_PATH,
                 strtoargv("dump 127.0.0.1 1234"));

  for (size_t i = 0; i < 2018; ++i) {
    write_pipe(pipes, 1, 0, "k%04lu climb over the sunset\n", i);
    write_pipe(pipes, 1, 0, "v%04lu climb over the sunset\n", i);
    expect_pipe(pipes, 1, 1, "OK\n");
  }

  expect_pipe(pipes, 3, 1, "IP port?\n");
  write_pipe(pipes, 3, 0, "127.0.0.1 1234\n");

  char actual[100];
  for (size_t i = 0; i < 2018; ++i) {
    if (fgets(actual, sizeof(actual), pipes.fs[3][1]) == NULL)
      ck_abort_msg("Fail at line %lu\n", i);
  }

  kill_pipeset_programs(pipes);
  remove_file("servers.txt");
}
END_TEST

typedef const char *ccp;
#define TEST_CAT(target, success, result, nparts, parts)                       \
  do {                                                                         \
    ccp *p = parts;                                                            \
    run_program(3, &pipes, PPS_CLIENT_CAT_BIN_PATH, NO_ARGS_ARR);              \
    for (size_t ri = 0; ri < nparts; ++ri) {                                   \
      write_pipe(pipes, 3, 0, "%s\n", p[ri]);                                  \
    }                                                                          \
    write_pipe(pipes, 3, 0, target "\n");                                      \
    TERMINATE_PIPE(pipes.fs[3][0]);                                            \
    if (success) {                                                             \
      expect_pipe(pipes, 3, 1, "OK\n");                                        \
      write_pipe(pipes, 2, 0, target "\n");                                    \
      expect_pipe(pipes, 2, 1, "OK " result "\n");                             \
    } else {                                                                   \
      expect_pipe(pipes, 3, 1, "FAIL\n");                                      \
      write_pipe(pipes, 2, 0, target "\n");                                    \
      expect_pipe(pipes, 2, 1, "FAIL\n");                                      \
    }                                                                          \
  } while (0)

START_TEST(pps_client_cat) {
  create_file("servers.txt", "127.0.0.1 1234\n");
  pipeset pipes = create_pipeset(4);
  launch_program(&pipes, SERVER_BIN_PATH, NO_ARGS_ARR);

  expect_pipe(pipes, 0, 1, "IP port?\n");
  write_pipe(pipes, 0, 0, "127.0.0.1 1234\n");
  expect_pipe(pipes, 0, 2, "Server listening on 127.0.0.1:1234.\n");

  launch_program(&pipes, PPS_CLIENT_PUT_BIN_PATH, strtoargv("put -n 1 -w 1"));
  launch_program(&pipes, PPS_CLIENT_GET_BIN_PATH, strtoargv("get -n 1 -r 1"));

  ADD_AND_CHECK("coming", "bicycle ");
  ADD_AND_CHECK("again", "races");
  ADD_AND_CHECK("a", "alpha");
  ADD_AND_CHECK("b", "beta");

  // Concat 2
  TEST_CAT("queen", 1, "bicycle races", 2, ((ccp[]){"coming", "again"}));
  // Copy
  TEST_CAT("jazz", 1, "bicycle races", 1, ((ccp[]){"queen"}));
  // Reference unexisting key
  TEST_CAT("masconi", 0, "", 2, ((ccp[]){"dsa", "jazz"}));

  TEST_CAT("c", 1, "alphabeta", 2, ((ccp[]){"a", "b"}));
  TEST_CAT("e", 0, "", 2, ((ccp[]){"a", "d"}));
  TEST_CAT("f", 1, "alpha", 1, ((ccp[]){"a"}));

  kill_pipeset_programs(pipes);
  remove_file("servers.txt");
}
END_TEST

#define TEST_SUBSTR(base, pos, lon, reg, op_success, verify_success, res)      \
  do {                                                                         \
    write_pipe(pipes, 3, 0, base "\n");                                        \
    write_pipe(pipes, 3, 0, pos "\n");                                         \
    write_pipe(pipes, 3, 0, lon "\n");                                         \
    write_pipe(pipes, 3, 0, reg "\n");                                         \
    expect_pipe(pipes, 3, 1, op_success ? "OK\n" : "FAIL\n");                  \
    if (verify_success) {                                                      \
      write_pipe(pipes, 2, 0, reg "\n");                                       \
      if (op_success)                                                          \
        expect_pipe(pipes, 2, 1, "OK " res "\n");                              \
      else                                                                     \
        expect_pipe(pipes, 2, 1, "FAIL\n");                                    \
    }                                                                          \
  } while (0)

START_TEST(pps_client_substr) {
  create_file("servers.txt", "127.0.0.1 1234\n");
  pipeset pipes = create_pipeset(4);
  launch_program(&pipes, SERVER_BIN_PATH, NO_ARGS_ARR);

  expect_pipe(pipes, 0, 1, "IP port?\n");
  write_pipe(pipes, 0, 0, "127.0.0.1 1234\n");
  expect_pipe(pipes, 0, 2, "Server listening on 127.0.0.1:1234.\n");

  launch_program(&pipes, PPS_CLIENT_PUT_BIN_PATH, strtoargv("put -n 1 -w 1"));
  launch_program(&pipes, PPS_CLIENT_GET_BIN_PATH, strtoargv("get -n 1 -r 1"));

  ADD_AND_CHECK("a", "123456");
  ADD_AND_CHECK("ex", "exemple");

  launch_program(&pipes, PPS_CLIENT_SUBSTR_BIN_PATH,
                 strtoargv("-n 1 -w 1 -r 1"));
  TEST_SUBSTR("a", "3", "2", "b", 1, 1, "45");
  TEST_SUBSTR("a", "-4", "3", "c", 1, 1, "345");

  TEST_SUBSTR("ex", "0", "2", "d", 1, 1, "ex");
  TEST_SUBSTR("ex", "1", "1", "e", 1, 1, "x");
  TEST_SUBSTR("ex", "4", "3", "f", 1, 1, "ple");
  TEST_SUBSTR("ex", "-1", "1", "g", 1, 1, "e");
  TEST_SUBSTR("ex", "-2", "2", "h", 1, 1, "le");

  TEST_SUBSTR("ex", "2", "0", "i", 1, 0, "");
  TEST_SUBSTR("ex", "-2", "0", "j", 1, 0, "");
  TEST_SUBSTR("ex", "5", "3", "k", 0, 1, "FAIL");
  TEST_SUBSTR("ex", "-1", "2", "l", 0, 1, "FAIL");

  TERMINATE_PIPE(pipes.fs[3][0]); // ^D

  kill_pipeset_programs(pipes);
  remove_file("servers.txt");
}
END_TEST

#define TEST_FIND(haystack, needle, res)                                       \
  do {                                                                         \
    write_pipe(pipes, 3, 0, haystack "\n");                                    \
    write_pipe(pipes, 3, 0, needle "\n");                                      \
    expect_pipe(pipes, 3, 1, "OK " res "\n");                                  \
  } while (0)

START_TEST(pps_client_find) {
  create_file("servers.txt", "127.0.0.1 1234\n");
  pipeset pipes = create_pipeset(4);
  launch_program(&pipes, SERVER_BIN_PATH, NO_ARGS_ARR);

  expect_pipe(pipes, 0, 1, "IP port?\n");
  write_pipe(pipes, 0, 0, "127.0.0.1 1234\n");
  expect_pipe(pipes, 0, 2, "Server listening on 127.0.0.1:1234.\n");

  launch_program(&pipes, PPS_CLIENT_PUT_BIN_PATH, strtoargv("put -n 1 -w 1"));
  launch_program(&pipes, PPS_CLIENT_GET_BIN_PATH, strtoargv("get -n 1 -r 1"));

  ADD_AND_CHECK("a", "123456123");
  ADD_AND_CHECK("b", "34");
  ADD_AND_CHECK("c", "67");
  ADD_AND_CHECK("d", "123");

  launch_program(&pipes, PPS_CLIENT_FIND_BIN_PATH, strtoargv("-n 1 -w 1 -r 1"));
  TEST_FIND("a", "b", "2");
  TEST_FIND("a", "c", "-1");
  TEST_FIND("a", "d", "0");

  kill_pipeset_programs(pipes);
  remove_file("servers.txt");
}
END_TEST

static Suite *suite(void) {
  Suite *s = suite_create("e2e-tests");

  TCase *tc_ht = tcase_create("Core");
  tcase_add_test(tc_ht, week07_handout_tests);
  tcase_add_test(tc_ht, pps_dump_node);
  tcase_add_test(tc_ht, pps_client_cat);
  tcase_add_test(tc_ht, pps_client_substr);
  tcase_add_test(tc_ht, pps_client_find);

  TCase *tc_stress = tcase_create("Stress Testing");
  tcase_add_test(tc_ht, pps_dump_node_stress_test);

  suite_add_tcase(s, tc_ht);
  suite_add_tcase(s, tc_stress);
  return s;
}

TEST_SUITE(suite)
