#include "node_list.h"
#include "config.h"
#include "error.h"
#include "myutil.h"
#include "node.h"
#include "util.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define true 1
#define false 0

// ======================================================================
node_list_t *node_list_new(void) {
  node_list_t *n_list = malloc(sizeof(node_list_t));
  M_FATAL_IF_NULL(n_list, sizeof(node_list_t), "node_list_new");
  /* CORRECTEUR: Helper functions should not make the program crash (even if
   * failed memory allocation is indeed serious) - propagate the error upwards
   * by returning NULL [-0.5 robustness]
   */
  (void)memset(n_list, 0, sizeof(node_list_t)); /*initialize memory*/
  /* CORRECTEUR: Again, `calloc` would make this redundant */
  return n_list;
}

// ======================================================================
node_list_t *get_nodes(void) {
  node_list_t *n_list = node_list_new(); /*create new empty node list*/

  FILE *fp = open_file_readonly(PPS_SERVERS_LIST_FILENAME);
  M_CLOSURE_EXIT_IF_NULL(fp, free(n_list), sizeof(FILE *), "Open server list.");

  n_list->nb_nodes = count_file(fp).nb_lines;

  /*allocate memory for list of nodes of size nb_nodes*/
  n_list->list = malloc(n_list->nb_nodes * sizeof(node_t *));
  M_CLOSURE_EXIT_IF_NULL(n_list->list, free(n_list),
                         n_list->nb_nodes * sizeof(node_t *),
                         "Allocate node list");

  for (size_t idx = 0; idx < n_list->nb_nodes; ++idx) {
    n_list->list[idx] = malloc(sizeof(node_t));
    M_CLOSURE_EXIT_IF_NULL(n_list->list[idx], node_list_free(n_list),
                           sizeof(node_t), "Create nodes");
  }

  rewind(fp); /*rewind to beggining of file*/
  /* CORRECTEUR: Why not make a single loop where you initialise the nodes
   * immediately?
   */
  char line[LINE_SIZE];
  char ip[IP_SIZE + 1];
  uint16_t port = 0;
  error_code err = ERR_NONE;

  for (size_t i = 0; i < n_list->nb_nodes; ++i) {
    get_str_file(line, LINE_SIZE, fp);
    port = 0;
    err = read_ip_port(line, ip, IP_SIZE, &port, max_PORT) == 0
              ? ERR_NONE
              : ERR_BAD_PARAMETER;
    M_FATAL_CLOSURE_IF(err != ERR_NONE, node_list_free(n_list), err,
                       "get_nodes",
                       "Invalid ipaddr/port specification in line %lu", i + 1);

    err = node_init(n_list->list[i], ip, port, 0);
    M_FATAL_CLOSURE_IF(err != ERR_NONE, node_list_free(n_list), err,
                       "get_nodes", "Initializing %lu node", i + 1);
  }

  fclose(fp);
  return n_list;
}

// ======================================================================
void node_list_free(node_list_t *n_list) {
  if (n_list != NULL) {
    if (n_list->list != NULL) {
      for (size_t i = 0; i < n_list->nb_nodes; i++) {
        if (n_list->list[i] != NULL) {
          node_end(n_list->list[i]);
          free(n_list->list[i]);
        }
      }
      free(n_list->list);
      n_list->list = NULL;
    }
    n_list->nb_nodes = 0;
    free(n_list);
    n_list = NULL;
    /* CORRECTEUR: This line has absolutely no effect. */
  }
}

// ======================================================================
error_code node_list_add(node_list_t *n_list, node_t node) {
  M_REQUIRE_NON_NULL(n_list);
  M_REQUIRE_NON_NULL(n_list->list);

  error_code err = ERR_NONE;
  ++n_list->nb_nodes;

  node_t **const old_n_list = n_list->list;
  if ((n_list->nb_nodes > SIZE_MAX / sizeof(node_t *)) ||
      ((n_list->list = realloc(n_list->list,
                               n_list->nb_nodes * sizeof(node_t *))) == NULL)) {
    n_list->list = old_n_list;
    --n_list->nb_nodes;
    M_EXIT(ERR_NOMEM, "Unable add extra node to list of size %zu",
           n_list->nb_nodes);
  } else {
    n_list->list[n_list->nb_nodes - 1] = &node;
  }

  return err;
}
