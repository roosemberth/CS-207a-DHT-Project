#include <errno.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>

#include "args.h"
#include "client.h"
#include "config.h"
#include "error.h"
#include "myutil.h"
#include "node_list.h"
#include "system.h"

void client_end(client_t *client) {
  close(client->socket);
  if (client->args != NULL) {
    free(client->args);
    client->args = NULL;
  }
  if (client->nodes != NULL) {
    node_list_free(client->nodes);
  }
}

error_code client_init(client_init_args_t cli_init) {
  client_t *client = cli_init.client;

  /*Initialize socket*/
  client->socket = get_socket((time_t)1); // timeout = 1 second

  char ***argv_ptr = cli_init.argv_ptr;
  size_t argc = cli_init.argc;

  // const char* const prog_name = argv[0];
  /*Supress program name*/
  fflush(stdout);

  ++(*argv_ptr);
  --argc;

  if (cli_init.arg_required != SIZE_MAX) {
    M_CLOSURE_EXIT_IF(argc < cli_init.arg_required, close(client->socket),
                      ERR_BAD_PARAMETER, "client_init",
                      "did not respect %zu mandatory arguments",
                      cli_init.arg_required);
  }

  size_t arg_opt = argc - cli_init.arg_required;
  if (arg_opt > 0) {
  /* CORRECTEUR: What if arg_required = 2, and the two options are `-n` `5` ?
   * [-1 corner case]
   */
    char **argv_prev = *argv_ptr;
    args_t *args = parse_opt_args(cli_init.supported_args, argv_ptr);
    M_CLOSURE_EXIT_IF(
        args == NULL, close(client->socket), ERR_IO, "client_init",
        "Error in parsing the (at most %zu) optional arguments", arg_opt);

    client->args = args;
    ptrdiff_t dp = *argv_ptr - argv_prev;
    argc -= (size_t)dp;
  } else {
    client->args = malloc(sizeof(args_t));
    M_CLOSURE_EXIT_IF_NULL(client->args, close(client->socket), sizeof(args_t),
                           "client->args alloc");
    client->args->N = 3;
    client->args->R = 2;
    client->args->W = 2;
    /* CORRECTEUR: Default values are a bit more tricky than this... */
  }

  if (cli_init.arg_required != SIZE_MAX) {
    // not pps-client-cat
    M_CLOSURE_EXIT_IF(argc != cli_init.arg_required, client_end(client),
                      ERR_BAD_PARAMETER, "client_init",
                      "did not respect %zu mandatory arguments",
                      cli_init.arg_required);
  } else {
    // pps-client-cat
    M_CLOSURE_EXIT_IF(argc < 2, client_end(client), ERR_BAD_PARAMETER,
                      "client_init",
                      "did not respect at least %d mandatory arguments", 2);
  }

  /*Initialize server corresponding to client*/
  client->nodes = get_nodes();

  M_CLOSURE_EXIT_IF((client->nodes == NULL), client_end(client), ERR_NETWORK,
                    "client_init", "Unable to Initialize nodes: %s",
                    strerror(errno));

  int test = ((client->args->N > client->nodes->nb_nodes) ||
              (client->args->R > client->args->N) ||
              (client->args->W > client->args->N));

  M_CLOSURE_EXIT_IF(test, client_end(client), ERR_BAD_PARAMETER, "client_init",
                    "(N(=%zu) > S) or (R(=%zu) > N) or (W(=%zu) > N)",
                    client->args->N, client->args->R, client->args->W);

  return ERR_NONE;
}
