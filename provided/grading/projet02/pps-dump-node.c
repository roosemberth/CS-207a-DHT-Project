#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "client.h"
#include "error.h"
#include "myutil.h"
#include "node.h"
#include "system.h"

int main(int argc, char **argv) {
  // jump program name
  --argc;
  ++argv;
  // exit if num argument different than 2
  M_FATAL_IF(argc != 2, ERR_BAD_PARAMETER, "pps-dump-node",
             "did not respect %d mandatory arguments", 2);
  uint16_t pps_port = 0;
  const size_t ip_addr_buffer_size = 16; // xxx.xxx.xxx.xxx\0 = 16
  char pps_ip[ip_addr_buffer_size];
  M_FATAL_IF(cmd_line_ip_port(argv, pps_ip, ip_addr_buffer_size, &pps_port) !=
                 0,
             ERR_IO, "pps-dump-node", "Get ip addr: %s", strerror(errno));

  node_t *node = malloc(sizeof(node_t));
  M_CLOSURE_EXIT_IF_NULL(node, free(node), sizeof(node_t), "pps-dump-node");

  error_code err = node_init(node, pps_ip, pps_port, 0);
  M_CLOSURE_EXIT_IF(err != ERR_NONE, free(node), err, "pps-dump-node",
                    "Initialize node: %s", strerror(errno));

  // if server does not reply within timeout, print FAIL:
  int socket = get_socket((time_t)1); // timeout = 1 second

  char buf_send[] = "\0";
  struct sockaddr *dest_addr = (struct sockaddr *)node->addr;
  ssize_t bytes = sendto(socket, buf_send, 1, 0, dest_addr, sizeof(*dest_addr));
  M_CLOSURE_EXIT_IF(bytes < 0, free(node), err, "pps-dump-node",
                    "Query node: %s", strerror(errno));

  char recv_buf[UDP_MTU + 1];
  char *dptr = recv_buf;

  size_t n_pairs = 0;
  size_t pairs_remaining = 0;

  do {
    memset(recv_buf, '\0', UDP_MTU + 1);
    bytes = recv(socket, recv_buf, UDP_MTU, 0);
    M_CLOSURE_EXIT_IF(bytes <= 0, free(node), err, "pps-dump-node",
                      "Awaiting from node reply: %s", strerror(errno));
    size_t r = (size_t)bytes + 1; // Extra byte set to zero to ease parsing.

    if (n_pairs == 0) { // Initial block
      n_pairs = ntohl(*(uint32_t *)recv_buf);
      pairs_remaining = n_pairs;

      // Avoid pointer arithmetics.
      dptr += sizeof(uint32_t) / sizeof(char);
      r -= sizeof(uint32_t) / sizeof(char);
    }

    do {
      char *key = dptr;
      char *value = memchr(key, '\0', r);
      M_CLOSURE_EXIT_IF(value == NULL, free(node), ERR_IO, "pps-dump-node",
                        "Parse k response %lu/%lu", pairs_remaining, n_pairs);
      ++value; // Character after '\0'
      r -= strlen(key) + 1;

      dptr = memchr(value, '\0', r);
      M_CLOSURE_EXIT_IF(dptr == NULL, free(node), ERR_IO, "pps-dump-node",
                        "Parse v response %lu/%lu", pairs_remaining, n_pairs);
      ++dptr;
      r -= strlen(value) + 1;
      printf("%s = %s\n", key, value);
      fflush(stdout);
    } while (r > 0);
  } while (pairs_remaining > 0);

  free(node);
  return 0;
}
/* CORRECTEUR: Consider modularising this function a bit (it's a single 70+ SLOC
 * function) [-0.5 style]
 */
