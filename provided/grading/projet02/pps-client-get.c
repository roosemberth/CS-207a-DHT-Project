#include <inttypes.h>
#include <stdio.h>

#include "client.h"
#include "error.h"
#include "hashtable.h"
#include "myutil.h"
#include "network.h"
#include "node.h"
#include "system.h"
#include "util.h"

int main(int argc, char **argv) {
  client_t client;
  memset(&client, 0, sizeof(client));
  MAYBE_FAIL(
      client_init((client_init_args_t){&client, 1, TOTAL_SERVERS | GET_NEEDED,
                                       (size_t)argc, &argv}) != ERR_NONE,
      "pps-client-get: client_init");

  pps_value_t value;

  MAYBE_FAIL(network_get(client, argv[0], &value) != ERR_NONE,
             "pps-client-get: send");
  printf("OK %s\n", value);
  fflush(stdout);

  return 0;
}
