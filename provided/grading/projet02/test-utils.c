#define _POSIX_SOURCE

#define _XOPEN_SOURCE 500

#include <assert.h>
#include <check.h>
#include <errno.h>
#include <poll.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/prctl.h>
#include <unistd.h>

#include "test-utils.h"

static int **allocate_2d_int(size_t x, size_t y) {
  int **t = malloc(sizeof(int *) * x);
  for (size_t i = 0; i < x; ++i) {
    t[i] = malloc(sizeof(int) * y);
    for (size_t j = 0; j < y; ++j) {
      t[i][j] = 0;
    }
  }
  return t;
}

pipeset create_pipeset(size_t n_pipes) {
  pipeset pipes;
  pipes.n_pipes = n_pipes;
  pipes.fd = allocate_2d_int(n_pipes, 3);
  pipes.ch = allocate_2d_int(n_pipes, 3);
  pipes.fs = malloc(sizeof(FILE **) * n_pipes);
  for (size_t i = 0; i < n_pipes; ++i) {
    pipes.fs[i] = malloc(sizeof(FILE *) * 3);
    for (size_t j = 0; j < 3; ++j)
      pipes.fs[i][j] = NULL;
  }
  pipes.next_idx = 0;
  pipes.pids = malloc(sizeof(pid_t) * n_pipes);

  return pipes;
}

void run_program(size_t program_idx, pipeset *pipes, const char *path,
                 char *const argv[]) {
  // Maybe close previously running pipes on this spot.
  if (pipes->fs[program_idx][0] != NULL)
    fclose(pipes->fs[program_idx][0]);
  if (pipes->fs[program_idx][1] != NULL)
    fclose(pipes->fs[program_idx][1]);
  if (pipes->fs[program_idx][2] != NULL)
    fclose(pipes->fs[program_idx][2]);
  // Do not kill the program on purpose:
  // Dead pid references might have been reclycled by the OS.

  int pipe_endpoints[2];
  FILE *f;
  int pfd;
  if (pipe(pipe_endpoints)) // stdin
    ck_abort_msg("Could not initialize pipe: %d:%d", program_idx, 0);
  pipes->fd[program_idx][0] = pfd = pipe_endpoints[1];
  pipes->ch[program_idx][0] = pipe_endpoints[0];
  pipes->fs[program_idx][0] = f = fdopen(pfd, "w"); // parent writes
  if (f == NULL)
    ck_abort_msg("Opening fd: %d", pfd);

  if (pipe(pipe_endpoints)) // stdout
    ck_abort_msg("Could not initialize pipe: %d:%d", program_idx, 1);
  pipes->fd[program_idx][1] = pipe_endpoints[0];
  pipes->ch[program_idx][1] = pipe_endpoints[1];
  pipes->fs[program_idx][1] = fdopen(pipe_endpoints[0], "r"); // parent reads
  if (pipe(pipe_endpoints))                                   // stderr
    ck_abort_msg("Could not initialize pipe: %d:%d", program_idx, 2);
  pipes->fd[program_idx][2] = pipe_endpoints[0];
  pipes->ch[program_idx][2] = pipe_endpoints[1];
  pipes->fs[program_idx][2] = fdopen(pipe_endpoints[0], "r"); // parent reads

  pid_t pid = fork();
  if (pid == 0) {                       // Child process
    dup2(pipes->ch[program_idx][0], 0); // Rewire stdin: by convention fd 0.
    dup2(pipes->ch[program_idx][1], 1); // Rewire stdout: by convention fd 1.
    dup2(pipes->ch[program_idx][2], 2); // Rewire stderr: by convention fd 2.

    for (size_t fd = 0; fd < 3; ++fd) { // Close fds.
      close(pipes->fd[program_idx][fd]);
      close(pipes->ch[program_idx][fd]);
    }

    prctl(PR_SET_PDEATHSIG, SIGKILL);

    // This call should never return!
    int r = execv(path, argv);
    ck_abort_msg("execve failed: %s", strerror(r));
  }

  pipes->pids[program_idx] = pid;
  close(pipes->ch[program_idx][0]); // Close the child-side of the stdin.
  close(pipes->ch[program_idx][1]); // Close the child-side of the stdout.
  close(pipes->ch[program_idx][2]); // Close the child-side of the stderr.
}

void launch_program(pipeset *pipes, const char *path, char *const argv[]) {
  size_t program_idx = pipes->next_idx++;
  run_program(program_idx, pipes, path, argv);
}

void kill_pipeset_programs(pipeset pipes) {
  for (size_t i = 0; i < pipes.n_pipes; ++i) {
    pid_t pid = pipes.pids[i];
    if (pid > 0)
      kill(pid, SIGKILL);
  }
  // TODO: Consider replacing the above loop with the line bellow.
  // kill(0, SIGKILL); // Kill all processed in this processe's group.
}

void expect_pipe(const pipeset pipes, size_t pipe_idx, size_t stream_idx,
                 const char *expected_fmt, ...) {
  char expected[1024];
  {
    va_list args;
    va_start(args, expected_fmt);
    vsnprintf(expected, sizeof(expected), expected_fmt, args);
    va_end(args);
  }

  FILE *f = pipes.fs[pipe_idx][stream_idx];
  int fd = pipes.fd[pipe_idx][stream_idx];

  if (ferror(f))
    clearerr(f);

  if (feof(f)) {
    int r = poll(&(struct pollfd){.fd = fd, .events = POLLIN}, 1, 5000);
    if (r == 0)
      ck_abort_msg("Timeout on fd %d. Expected message: %s", fd, expected);
    else if (r < 0)
      ck_abort_msg("Error on fd %d: %s. Expected message: %s", fd,
                   strerror(errno), expected);
  }

  char actual[1024];
  if (fgets(actual, sizeof(actual), f) == NULL)
    ck_abort_msg("Could not read from fd %d", fd);

  ck_assert_str_eq(expected, actual);
}

void write_pipe(const pipeset pipes, size_t pipe_idx, size_t stream_idx,
                const char *fmt, ...) {
  char message[1024];
  {
    va_list args;
    va_start(args, fmt);
    vsnprintf(message, sizeof(message), fmt, args);
    va_end(args);
  }

  FILE *f = pipes.fs[pipe_idx][stream_idx];
  int fd = pipes.fd[pipe_idx][stream_idx];

  int r = poll(&(struct pollfd){.fd = fd, .events = POLLOUT}, 1, 5000);

  {
    if (r == 0)
      ck_abort_msg("Timeout on fd %d while writing %s", fd, message);
    else if (r < 0)
      ck_abort_msg("Error on fd %d: %s. While writing %s", fd, strerror(errno),
                   message);
  }

  fprintf(f, "%s", message);
  fflush(f);
}

void create_file(const char *name, const char *contents) {
  FILE *f = fopen(name, "w");
  if (f == NULL)
    ck_abort_msg("Could not create file %s", name);
  fprintf(f, "%s", contents);
  fclose(f);
}

void remove_file(const char *name) {
  if (remove(name))
    ck_abort_msg("Could not remove servers.txt");
}

char **strtoargv(const char *str) {
  size_t nargs = 0;
  for (char *search = strdup(str); search != NULL;
       search = strchr(search + 1, ' '))
    ++nargs;

  char **argvp = malloc(sizeof(char *) * (nargs + 1)); // Trailing NULL
  size_t argc = 0;
  for (char *token = strtok(strdup(str), " "); token != NULL;
       token = strtok(NULL, " ")) {
    argvp[argc++] = strdup(token);
  }
  argvp[argc] = NULL;
  assert(argc <= nargs);
  return argvp;
}
