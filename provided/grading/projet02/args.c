#include "args.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define true 1
#define false 0
typedef int flag;
typedef enum { N, W, R, NONE } Option;

args_t *parse_opt_args(size_t supported_args, char ***rem_argv) {
  args_t *args = malloc(sizeof(args_t));
  if (args == NULL) {
    fprintf(stderr, "error in args_t memory allocation\n");
    return NULL;
  }

  // default values:
  args->N = 3;
  args->R = 2;
  args->W = 2;

  Option option = NONE;
  flag new_option = true;

  while ((*rem_argv)[0] != NULL) {
    if (new_option == true) {
      if (strcmp((*rem_argv)[0], "-n") == 0) {
        if (supported_args & TOTAL_SERVERS) {
          option = N;
        } else {
          fprintf(stderr, "Invalid option -n\n");
          free(args);
          args = NULL;
          return args;
          /* CORRECTEUR: if -n is not in supported_args, it must be treated as a
           * positional argument, not as an error [-0.5 correctness]
           */
        }
      } else if (strcmp((*rem_argv)[0], "-w") == 0) {
        if (supported_args & PUT_NEEDED) {
          option = W;
        } else {
          fprintf(stderr, "Invalid option -w\n");
          free(args);
          args = NULL;
          return args;
        }
      } else if (strcmp((*rem_argv)[0], "-r") == 0) {
        if (supported_args & GET_NEEDED) {
          option = R;
        } else {
          fprintf(stderr, "Invalid option -r\n");
          free(args);
          args = NULL;
          return args;
        }
      } else if (strcmp((*rem_argv)[0], "--") == 0) {
        ++(*rem_argv);
        return args;
      } else {
        return args;
      }
      new_option = false;
    } else {
      int i;
      int j;
      j = sscanf((*(rem_argv)[0]), "%d", &i);
      if ((j != 1) || (i <= 0)) {
        fprintf(stderr, "value for one of the options is invalid\n");
        free(args);
        args = NULL;
        return args;
      } else {
        switch (option) {
        case N:
          args->N = (size_t)i;
          break;
        case W:
          args->W = (size_t)i;
          break;
        case R:
          args->R = (size_t)i;
          break;
        default:
          fprintf(stderr, "Invalid option?\n");
          free(args);
          args = NULL;
          return args;
        }
      }
      new_option = true;
    }
    // next argument
    ++(*rem_argv);
  }
  /* CORRECTEUR: Only -n 1 should be accepted (with R and/or W adapted
   * accordingly) [-1 correctness]
   */

  if (new_option == false) {
    // all arguments were seen and missing value for option
    free(args);
    args = NULL;
    return args;
  }
  // all arguments were seen
  return args;
}
/* CORRECTEUR: Consider splitting this up into multiple functions. It's a 90+
 * SLOC long block of minimalistically indented code stuffed into a single
 * function. [-1 style]
 */
