#include <stdio.h>
#include <stdlib.h>

#include "client.h"
#include "error.h"
#include "hashtable.h"
#include "myutil.h"
#include "network.h"
#include "util.h"

/**
 * The last key specified is the target register, which might not exist. This is
 * not an error.
 * If the previous register did not exist “schedule” the error for later.
 * This scheduling is done using the variable err, which is only updated if no
 * previous error has been detected and we want to signal something other than
 * ERR_NONE
 */
/* CORRECTEUR: Why not simply treat the X-1 arguments as "must exist", and write
 * the result to the Xth register without requiring a read there? This way you
 * wouldn't need to keep around 3 (three!) error codes to backtrack potential
 * errors...
 */
#define ERROR_GUARD(expr)                                                      \
  do {                                                                         \
    last_err = expr;                                                           \
    if (prev_err != ERR_NONE && err == ERR_NONE)                               \
      err = prev_err;                                                          \
    prev_err = last_err;                                                       \
  } while (0)

#define CHECK_ERROR()                                                          \
  do {                                                                         \
    if (err != ERR_NONE) {                                                     \
      printf("FAIL\n");                                                        \
      M_EXIT_IF_ERR(err, "pps-client-cat");                                    \
    }                                                                          \
  } while (0)

int main(int argc, char **argv) {
  client_t client;
  memset(&client, 0, sizeof(client));
  MAYBE_FAIL(client_init((client_init_args_t){&client, SIZE_MAX, ALL_ARGS,
                                              (size_t)argc, &argv}) != ERR_NONE,
             "pps-client-cat: client_init");
  char acc[LINE_SIZE] = {0};
  pps_value_t last;
  pps_value_t prev;

  size_t argc_req = argv_size(argv);

  // See ERROR_GUARD...
  error_code prev_err = ERR_NONE;
  error_code last_err = ERR_NONE;
  error_code err = ERR_NONE;

  ERROR_GUARD(network_get(client, argv[0], &prev));

  for (size_t i = 1; i < argc_req; ++i) {
    ERROR_GUARD(network_get(client, argv[i], &last));
    if (last != NULL && strlen(acc) + strlen(last) > LINE_SIZE) {
      // Error: Too long!
      /* CORRECTEUR: ... and how is this handled? [-0.5 robustness] */
    }
    if (prev != NULL) {
      strcat(acc, prev);
      my_free_const_ptr(prev);
    }
    prev = last;
  }
  CHECK_ERROR();

  err = network_put(client, argv[argc_req - 1], acc);
  CHECK_ERROR();

  printf("OK\n");

  return 0;
}
