#pragma once

/**
 * @file client.h
 * @brief Client definition and related functions
 *
 * @author Valérian Rousset
 */

#include <stddef.h> // for size_t

#include "args.h"      // weeks 10 and after
#include "error.h"     // for error_code type
#include "node.h"      // in week 5
#include "node_list.h" // weeks 6 to 10

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include "ring.h" // weeks 11 and after
#pragma GCC diagnostic pop

/**
 * @brief client state
 */
typedef struct {
  const char *nom;
  node_list_t *nodes;
  int socket;
  args_t *args;
} client_t;

/**
 * @brief client_init function arguments.
 *        To be defined in week 05 and THEN UPDATED in week 10.
 */
typedef struct {
  client_t *client;
  size_t arg_required;
  size_t supported_args;
  size_t argc;
  char ***argv_ptr;
} client_init_args_t;

/**
 * @brief does all the work to be done at the end of life of a client
 * @param client the client to end
 */
void client_end(client_t *client);

/**
 * @brief does all the work to be done at the beginning of life of a client
 * @param client the client to initialize
 * @return some error code, if any.
 */
error_code client_init(client_init_args_t cli_init);
