#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#define DEBUG 1
#include "client.h"
#include "error.h"
#include "hashtable.h"
#include "myutil.h"
#include "network.h"
#include "node.h"
#include "system.h"
#include "util.h"

int main(int argc, char **argv) {
  client_t client;
  memset(&client, 0, sizeof(client));
  MAYBE_FAIL(
      client_init((client_init_args_t){&client, 2, TOTAL_SERVERS | PUT_NEEDED,
                                       (size_t)argc, &argv}) != ERR_NONE,
      "pps-client-put: client_init");

  // printf("key:value - %s:%s\n", argv[0], argv[1]);
  // printf("N: %zu, W: %zu, R: %zu\n", client.args->N, client.args->W,
  // client.args->R);
  MAYBE_FAIL(network_put(client, argv[0], argv[1]) != ERR_NONE,
             "pps-client-put: send");
  printf("OK\n");
  fflush(stdout);

  return 0;
}
