#include <arpa/inet.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "config.h"
#include "hashtable.h"
#include "myutil.h"
#include "system.h"

/** ======================================================================
 * @brief tool function to brutally exit on system error
 * @param error message identifier
 */
static void die(const char *s) {
  if (errno)
    perror(s);
  else
    fprintf(stderr, "%s failed for some unreported reason.\n", s);
  exit(1);
}

static void init_server(int socket_fd) {
  uint16_t pps_port = 0;
  const size_t ip_addr_buffer_size = 16; // xxx.xxx.xxx.xxx\0 = 16
  char *pps_ip = malloc(ip_addr_buffer_size);
  ask_ip_port(pps_ip, 16, &pps_port);

  // Bind server to the address:port
  int r = 0;
  if ((r = bind_server(socket_fd, pps_ip, pps_port))) {
    close(socket_fd);
    die("Opening socket");
  }

  fprintf(stderr, "Server listening on %s:%d.\n", pps_ip, pps_port);
  fflush(stderr);
  free(pps_ip);
}

static Htable_t table;

int main(int argc, char **argv) {
  // Set up socket (without timeout, for servers).
  table = construct_Htable(HTABLE_SIZE);
  int socket = get_socket((time_t)0); // per documentation 0 => inf
  init_server(socket);

  uint8_t buf[MAX_MSG_SIZE + 1]; // Extra '\0' when receiving max params.

  // Receive messages forever.
  while (1) {
    // Receive message and get return address.
    struct sockaddr_in cli_addr;
    socklen_t addr_len = sizeof(cli_addr);
    memset(&cli_addr, 0, addr_len);

    ssize_t recv_bytes = recvfrom(socket, buf, MAX_MSG_SIZE, 0,
                                  (struct sockaddr *)&cli_addr, &addr_len);
    assert(recv_bytes <= MAX_MSG_SIZE);
    if (recv_bytes < 0)
      die("recvfrom");

    if (recv_bytes == 0) {
      // Received '\0' -> Reply '\0' (for pps-list-nodes)
      sendto(socket, buf, 0, 0, (struct sockaddr *)&cli_addr, addr_len);
    } else if (recv_bytes == 1 && buf[0] == '\0') { // Dump node request.
      kv_list_t *kvs_head = dump_hashtable(&table);
      char buf2[UDP_MTU + 1]; // Extra '\0' when receiving max params.
      memset(buf2, 0, sizeof(buf2));
      size_t used_bytes = 0;

      uint32_t n_pairs = 0;
      for (kv_list_t *kvs = kvs_head; kvs->next != NULL; kvs = kvs->next) {
        if (kvs->pair != NULL && kvs->pair->key != NULL &&
            kvs->pair->value != NULL)
          ++n_pairs;
      }

      char *cursor = buf2;
      *(uint32_t *)cursor = htonl(n_pairs);
      cursor += sizeof(uint32_t) / sizeof(char);
      used_bytes += sizeof(uint32_t) / sizeof(char);

      for (kv_list_t *kvs = kvs_head; kvs != NULL; kvs = kvs->next) {
        kv_pair_t *pair = kvs->pair;
        if (pair->key == NULL || pair->value == NULL)
          continue;

        size_t required_bytes =
            strlen(pair->key) + strlen(pair->value) + 2; // +2 '\0' characters

        if (used_bytes + required_bytes > sizeof(buf2)) { // Transfer now!
          if (sendto(socket, buf2, used_bytes - 1 /* final '\0' */, 0,
                     (struct sockaddr *)&cli_addr, addr_len) < 0) {
            debug_print("Could not send reply [dump] (%lub)", used_bytes - 1);
          }
          cursor = buf2;
          used_bytes = 0;
        }
        sprintf(cursor, "%s", pair->key);
        cursor += strlen(pair->key) + 1;
        sprintf(cursor, "%s", pair->value);
        cursor += strlen(pair->value) + 1;
        used_bytes += required_bytes;
      }
      if (sendto(socket, buf2, used_bytes - 1 /* final '\0' */, 0,
                 (struct sockaddr *)&cli_addr, addr_len) < 0) {
        debug_print("Could not send final reply [dump] (%lub)", used_bytes - 1);
      }
    } else {
      uint8_t *split_addr;
      if ((split_addr = memchr(buf, '\0', (size_t)recv_bytes))) { // Put
        buf[recv_bytes] = '\0'; // String termination for value.
        pps_key_t key = (pps_key_t)buf;
        pps_value_t value = (pps_value_t)split_addr + 1;

        error_code err = add_Htable_value(table, key, value);
        if (err != ERR_NONE)
          debug_print("Could not store received values: %s: %s", key, value);

        // Ack
        sendto(socket, NULL, 0, 0, (struct sockaddr *)&cli_addr, addr_len);
      } else {                  // Get
        buf[recv_bytes] = '\0'; // String termination for key.
        pps_key_t key = (pps_key_t)buf;
        pps_value_t value = get_Htable_value(table, key);

        if (value == NULL) {
          char *v = malloc(sizeof(char) * 1);
          *v = '\0';
          value = v;
        }

        if (sendto(socket, value, value == NULL ? 0 : strlen(value), 0,
                   (struct sockaddr *)&cli_addr, addr_len) < 0)
          debug_print("Could not send answer: %s: %s", key,
                      value == NULL ? "NULL" : value);
      }
    }
  }

  return 0;
}
/* CORRECTEUR: Consider splitting this up into multiple functions. It's a 100+
 * SLOC long block of minimalistically indented code stuffed into a single
 * function, making it extremely hard to read. [-1 style]
 */
