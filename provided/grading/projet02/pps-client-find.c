#include <stdio.h>
#include <stdlib.h>

#include "client.h"
#include "error.h"
#include "hashtable.h"
#include "myutil.h"
#include "network.h"
#include "util.h"

int main(int argc, char **argv) {
  client_t client;
  memset(&client, 0, sizeof(client));
  MAYBE_FAIL(client_init((client_init_args_t){&client, 2, ALL_ARGS,
                                              (size_t)argc, &argv}) != ERR_NONE,
             "pps-client-find: client_init");

  pps_value_t base;
  pps_value_t subs;

  MAYBE_FAIL(network_get(client, argv[0], &base) != ERR_NONE,
             "pps-client-find: get base");
  MAYBE_FAIL(network_get(client, argv[1], &subs) != ERR_NONE,
             "pps-client-find: get sub");

  int substr_idx = -1;
  const char *substr_addr = strstr(base, subs);
  if (substr_addr != NULL)
    substr_idx = (int)(substr_addr - base);

  printf("OK %d\n", substr_idx);
  fflush(stdout);

  return 0;
}
