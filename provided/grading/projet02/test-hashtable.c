/**
 * @file test-hashtable.c
 * @brief test code for hashtables
 *
 * @author Valérian Rousset & Jean-Cédric Chappelier
 * @date 02 Oct 2017
 */

#include <stdio.h> // for puts(). to be removed when no longer needed.

#include <check.h>

#include "hashtable.h"
#include "test-utils.h"

#define TABLE_SIZE 10

static Htable_t table;

START_TEST(add_value_does_retrieve_same_value) {
  pps_key_t key = "42";
  pps_value_t expected =
      "Answer to the Ultimate Question of Life, the Universe, and Everything";
  pps_value_t actual;

  table = construct_Htable(TABLE_SIZE);
  error_code err = add_Htable_value(table, key, expected);
  if (err != ERR_NONE)
    ck_abort_msg("Add Htable value: %s", ERR_MESSAGES[err - ERR_NONE]);
  actual = get_Htable_value(table, key);

  ck_assert_str_eq(expected, actual);
}
END_TEST

static Suite *hashtable_suite(void) {

  Suite *s = suite_create("hashtable.h");

  TCase *tc_ht = tcase_create("hashtable");
  suite_add_tcase(s, tc_ht);

  tcase_add_test(tc_ht, add_value_does_retrieve_same_value);

  return s;
}

TEST_SUITE(hashtable_suite)
