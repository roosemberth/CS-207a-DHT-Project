#include <stdlib.h>
#include <string.h>

#include "myutil.h"
#include "node.h"
#include "system.h"

error_code node_init(node_t *node, const char *ip, uint16_t port,
                     size_t _unused node_id) {
  error_code err = ERR_NONE;
  node->addr = malloc(sizeof(struct sockaddr_in));
  /*specify node_id in the next error call, for the following weeks*/
  M_EXIT_IF_NULL(node->addr, (int)sizeof(struct sockaddr_in), "node_init");

  (void)memset(node->addr, 0, sizeof(struct sockaddr_in));
  err = get_server_addr(ip, port, node->addr);
  return err;
}

void node_end(node_t *node) {
  free(node->addr);
  node->addr = NULL;
}
