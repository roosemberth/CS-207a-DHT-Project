#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "error.h"
#include "myutil.h"
#include "node.h"
#include "node_list.h"
#include "system.h"
#include "util.h"

int main(int argc, char **argv) {

  M_FATAL_IF(argc != 1, ERR_BAD_PARAMETER, "pps-list-node",
             "did not respect %d mandatory arguments", 2);

  node_list_t *n_list = get_nodes();

  FILE *fp = open_file_readonly(PPS_SERVERS_LIST_FILENAME);
  M_CLOSURE_EXIT_IF(fp == NULL, node_list_free(n_list), ERR_IO,
                    "pps_list_nodes", "Opening servers file %s",
                    PPS_SERVERS_LIST_FILENAME);

  char line[LINE_SIZE];
  char ip[IP_SIZE + 1];
  uint16_t port = 0;
  // if server does not reply within timeout, print FAIL:
  int socket = get_socket((time_t)1); // timeout = 1 second

  error_code err = ERR_NONE;
  node_t *node = NULL;

  // This is a mock message, since length will be zero
  char buf_send[] = "\0";

  // Initialize received message buffer:
  char buf_rcv[MAX_MSG_ELEM_SIZE + 1];
  memset(buf_rcv, '\0', MAX_MSG_ELEM_SIZE + 1);

  for (size_t node_idx = 0; node_idx < n_list->nb_nodes; ++node_idx) {

    get_str_file(line, LINE_SIZE, fp);

    err = read_ip_port(line, ip, IP_SIZE, &port, max_PORT) == 0
              ? ERR_NONE
              : ERR_BAD_PARAMETER;
    M_CLOSURE_EXIT_IF(err != ERR_NONE, node_list_free(n_list), err, "cfg node",
                      "Invalid ipaddr/port spec in line %lu", node_idx + 1);

    node = n_list->list[node_idx];
    struct sockaddr *dest_addr = (struct sockaddr *)node->addr;
    ssize_t res = sendto(socket, buf_send, 0, 0, dest_addr, sizeof(*dest_addr));
    M_CLOSURE_EXIT_IF(res < 0, node_list_free(n_list), ERR_NETWORK, "ping node",
                      "Error sending to node %lu: %s", node_idx + 1,
                      strerror(errno));

    res = recv(socket, buf_rcv, MAX_MSG_ELEM_SIZE, 0);

    printf("%s %hu ", ip, port);
    if (res == 0) {
      printf("OK\n");
    } else if (res < 0) {
      printf("FAIL\n");
    }
    /* CORRECTEUR: And if res > 0? (Hint: given that we expect a 0-size message,
     * that would go under "misbehaviour")
     * [-0.5 robustness]
     */
  }

  fclose(fp);
  node_list_free(n_list);

  return 0;
}
