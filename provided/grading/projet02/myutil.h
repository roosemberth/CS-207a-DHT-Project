#pragma once

/**
 * @file myutil.h
 * @brief Some tool tools/functions
 *
 */

#include <inttypes.h> // for uint32_t scanf
#include <stddef.h>   // for size_t
#include <stdio.h>

#include "error.h"

#define LINE_SIZE 50000 // MAX_MSG_ELEM_SIZE = 32753
#define UDP_MTU 65507   // UDP MTU without jumbo frames.

/*Check inside network_put/get if the size of string sent is greater
 *then MAX_MSG_ELEM_SIZE */

/**
 * @brief Reads a string from a FILE
 * @param array of chars that stores response; Max length of string read
 * @return void
 */
error_code get_str_file(char *response, int size, FILE *fp);

/**
 * @brief Reads an int from input
 * @param pointer to int that stores value
 * @return void
 */
error_code askfor_int(int *i_ptr);

/**
 * @brief Basic datastructure to store line, word, and byte counts.
 *
 */
typedef struct {
  unsigned long int nb_lines;
  unsigned long int nb_words;
  unsigned long int nb_chars;
} counts;

/**
 * @brief Computes the number of lines, words and bytes of a files.
 * @param fp The (opened) file to count from.
 * @return the number of lines, words and bytes counts.
 */
counts count_file(FILE *f);

/**
 * @brief Opens a file readonly. Handles errors.
 * @param filename the name of the file to be opened.
 * @return the opened file.
 */
FILE *open_file_readonly(const char *filename);

/**
 * @brief reads ip and port from line
 * @param line: the line from which ip and port are read
 * @return 0 if successful and 1 otherwise
 */
int read_ip_port(const char *line, char *ip, size_t SIZE_ip, uint16_t *port,
                 const int MAX_port);

/**
 * @brief Asks the user for ip and port from stdin
 * @param pps_ip: Where to store the ip address as a string.
 * @param ip_size: max size of pps_ip buffer
 * @param pps_port: port destination buffer
 * @return 0 if successful and 1 otherwise
 */
int ask_ip_port(char *pps_ip, size_t ip_size, uint16_t *pps_port);

/**
 * @brief gets the user ip and port from command line arguments
 * @param pps_ip: Where to store the ip address as a string.
 * @param ip_size: max size of pps_ip buffer
 * @param pps_port: port destination buffer
 * @return 0 if successful and 1 otherwise
 */
int cmd_line_ip_port(char ** const argv, char *pps_ip, size_t ip_size,
                      uint16_t *pps_port);


/**
 * @brief free a const pointer.
 * Declared as function to avoir Wcast
 */
void my_free_const_ptr(const void *x);

// Utility macros

#define M_FATAL(fmt, ...)                                                      \
  do {                                                                         \
    debug_print(fmt, __VA_ARGS__);                                             \
    exit(1);                                                                   \
  } while (0)

#define M_FATAL_ERR(error_code, name, fmt, ...)                                \
  M_FATAL("%s: error: %s" fmt, name, ERR_MESSAGES[error_code - ERR_NONE],      \
          __VA_ARGS__)

#define M_FATAL_IF(test, error_code, name, fmt, ...)                           \
  do {                                                                         \
    if (test) {                                                                \
      M_FATAL_ERR(error_code, name, fmt, __VA_ARGS__);                         \
    }                                                                          \
  } while (0)

#define M_FATAL_CLOSURE_IF(test, closure, error_code, name, fmt, ...)          \
  do {                                                                         \
    if (test) {                                                                \
      { closure; }                                                             \
      M_FATAL_ERR(error_code, name, fmt, __VA_ARGS__);                         \
    }                                                                          \
  } while (0)

#define M_FATAL_IF_NULL(var, size, name)                                       \
  M_FATAL_IF(var == NULL, ERR_NOMEM, name,                                     \
             ", cannot allocate %lu bytes for %s", size, #var);

#define M_CLOSURE_EXIT_IF(test, closure, error_code, name, fmt, ...)           \
  do {                                                                         \
    if (test) {                                                                \
      { closure; }                                                             \
      M_EXIT_ERR(error_code, name, fmt, __VA_ARGS__);                          \
    }                                                                          \
  } while (0)

#define M_CLOSURE_EXIT_IF_NULL(var, closure, size, name)                       \
  do {                                                                         \
    if (var == NULL) {                                                         \
      { closure; }                                                             \
      M_EXIT_ERR(ERR_NONE, name, ", cannot allocate %lu bytes for %s", size,   \
                 #var);                                                        \
    }                                                                          \
  } while (0)

#define MAYBE_FAIL(cond, msg)                                                  \
  if (cond) {                                                                  \
    printf("FAIL\n");                                                          \
    fflush(stdout);                                                            \
    debug_print(msg ": %s\n", strerror(errno));                                \
    return 0 ;                                                         \
  }
  /* CORRECTEUR: MAYBE_FAIL evaluates errno after calling a bunch of other
   * things, so it's not accurate anymore (read the "Notes" section in errno(3))
   * and the error message is just garbage (which in turn makes it difficult to
   * debug why your code fails some tests) [-1 malus (correctness)]
   */
